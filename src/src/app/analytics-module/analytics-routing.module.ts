import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnalyticsMainComponent } from './analytics-main/analytics-main.component';

const routes: Routes = [

  { path: 'script/:script_id/branch/:branch_id/analyse', component: AnalyticsMainComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalyticsRoutingModule { }
