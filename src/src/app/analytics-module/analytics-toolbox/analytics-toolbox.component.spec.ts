import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyticsToolboxComponent } from './analytics-toolbox.component';

describe('AnalyticsToolboxComponent', () => {
  let component: AnalyticsToolboxComponent;
  let fixture: ComponentFixture<AnalyticsToolboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalyticsToolboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyticsToolboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
