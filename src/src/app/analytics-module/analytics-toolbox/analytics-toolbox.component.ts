import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
@Component({
  selector: 'app-analytics-toolbox',
  templateUrl: './analytics-toolbox.component.html',
  styleUrls: ['./analytics-toolbox.component.css'],
})
export class AnalyticsToolboxComponent implements OnInit {
  visitors = false ;
  drops = false;
  timestamps = false;
  @Output() analytics = new EventEmitter();
  @Output() dates = new EventEmitter();
  fromdate;
  todate;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'menu',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/menu.svg'));
      iconRegistry.addSvgIcon(
        'calendar',
        sanitizer.bypassSecurityTrustResourceUrl('assets/img/calendar.svg'));

  }

  ngOnInit() {
  }

showAnalytics() {
  this.analytics.emit([this.visitors , this.drops, this.timestamps]);
}

loadDates() {

  if (this.todate !== undefined) {

  if (this.fromdate !== undefined) {
   const from_date_object = this.fromdate;
   const from_date_formated = from_date_object.getFullYear() + ("0" + (from_date_object.getMonth() + 1)).slice(-2) + ("0" + from_date_object.getDate()).slice(-2);

   const to_date_object = this.todate;
   const to_date_formated = to_date_object.getFullYear() + ("0" + (to_date_object.getMonth() + 1)).slice(-2) + ("0" + to_date_object.getDate()).slice(-2);

   this.dates.emit(from_date_formated + '-' + to_date_formated);
  } else {
    const to_date_object = this.todate;
    const  to_date_formated = to_date_object.getFullYear() + ("0" + (to_date_object.getMonth() + 1)).slice(-2) + ("0" + to_date_object.getDate()).slice(-2);
    this.dates.emit(to_date_formated);
  }



}

}

}
