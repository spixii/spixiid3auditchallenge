import { Component, OnInit } from '@angular/core';
import { CoreService } from '../../core-module/core.service';
import { ActivatedRoute } from '@angular/router';
import { visitRootRenderNodes } from '@angular/core/src/view/util';
import { ViewEncapsulation } from '@angular/core';
import { GraphService } from '../../designer-module/graph.service';
import { MatDialog } from '@angular/material';


declare const d3;

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TreeComponent implements OnInit {
  script;  // current script
  branch;  // current branch
  treeData;  // nodes repsesented with children
  nodes;     // data of all the nodes of the tree
  root;     // the root of the tree
  margin;    // margin of tree
  width;    // width of the tree
  height;   //height of the tree
  coords;   // cordinates of the mouse position
  svg;      // the svg containing the tree
  tree;     // the tree d3 representation
  links;    // all the links of the tree
  nodeData;  // all the node data coming from the backend
  edgeData;  // all the edge data coming from the backend
  total_node_text = 0;
  entry = 'Q1';  // entry node
  visitorsColor = 0xDE; // basic color for displaying traffic
  showTraffic = false;
  showDrops = false;
  showTimestamps = false;
  response = false;  // whether a response is ready
  spinner = '#6576BF';  // color of the loading spinner
  todate;      // start of date range
  fromdate;    // end of date range
  constructor(public core: CoreService, private route: ActivatedRoute, public graphService:  GraphService ) { }

  ngOnInit() {
    this.script = this.route.snapshot.paramMap.get('script_id');     // get the script id from url
    this.branch = this.route.snapshot.paramMap.get('branch_id');
    this.loadTree();
  }

  async loadTree() {
    
    await this.core.loadPathsTree(this.script, this.branch).subscribe(    // obesrver that receives all the graph data from loopback
      response => {
        this.nodeData = response[0];   // save node data from db
        this.edgeData = response[1];    // save edge data from db
        this.getNodeData([this.nodeData, []]);  // save node data in a heirarchical stracture
        this.initialise();    // initialise the tree
        this.plotNodes();     // plot the tree nodes
        this.plotEdges();     // plot the tree edges
        this.plotLinkLabels();   // plot the labels of the links
        this.response = true;
      }, err => {
        console.log(err);
        alert('No data available');
      });
  }


getNodeData(data) {
  const nodeData = data[0];
  if (nodeData.length !== 0) {
    this.treeData = d3.stratify()   // create the graph data by taking the node id and the parent id
      .id(function (d) {
        return d.id; })
      .parentId(function (d) {
       if (d.parent) {
        
          return d.parent;
        }
      })
      (nodeData);

    // assign the name, param and label to each node
    this.treeData.each(function (d) {
      d.text = d.data.text;
    });
  }

}
initialise() {
const self = this;

  this.margin = { top: 39, right: 90, bottom: 30, left: 32 };
  this.width = 500 - this.margin.left - this.margin.right;
  this.height = 400 - this.margin.top - this.margin.bottom;
  const zoomed = () => self.svg.attr('transform', d3.event.transform);
  const zoom = d3.zoom().scaleExtent([0.1, 2]).on('zoom', zoomed);
  this.svg = d3.select('#treecontainer').append('svg')   // create the svg of the whole graph
    .attr('id', 'tree')
    .attr('width', '90%')
    .attr('height', '90%')
    .on('mouseover', (d) => {
      self.coords = d3.mouse(d3.event.currentTarget);
      d3.select('#tree').call(zoom);
    })
    .on('contextmenu', () => {
      d3.selectAll('.tooltip').remove();
      return false;
    })
    .append('g')   // create the class containg the graph
    .attr('class', 'group')
    .attr('id', 'g-main')
    .attr('transform', 'translate(150, 300)');

  //this.duration = 0;

  this.root = d3.hierarchy(this.treeData, (d) => d.children);  // create a tree with a root
  this.root.x0 = this.height / 2;
  this.root.y0 = this.width / 10;



  this.tree = d3.tree()   // create the conversation tree
    .size([this.height, this.width])
    .nodeSize([30, 0])   // the size of each node
    .separation(function separation(a, b) { return 12; });
    this.treeData = this.tree(this.root);
    this.nodes = this.treeData.descendants();   // get nodes from tree
    this.links = this.tree(this.treeData).links();  // get edges from tree




}



plotNodes() {
  const self = this;
  let id = 0;
  const node = this.svg.selectAll('g.node')  // create class containing the nodes
      .data(this.nodes, (d) => ++id);    // insert node data into the graph data
    this.nodes.forEach((d) => {   // set the cordinates of each node
      d.x0 = d.x;
      d.y0 = d.y;
    });


    const nodeEnter = node.enter().append('g')
      .attr('class', 'node')    // create node object
      .attr('id', (d) => d.data.id) // set its id

      this.nodes.forEach((d) => { d.y = d.y0 + (d.depth * 180) - (d.height * 22); d.x = (d.x0 / 2.8 + 45); });
      nodeEnter.append('path') // set the edge color
      .style('stroke', function (d) {
         return '#5D87BE'; 
         

      })
      .attr('class', 'nodesymbol')  // create the circle represeting the node
      .attr('id', (d) => `node_${d.data.id}`)
      .style('fill', function (d) { 
       

        return '#fff'; 
      })
      .attr('d', d3.symbol()
          .size( (d) => {
              return 500;
              })
          .type(function (d) {
            {
               return d3.symbolCircle;      // set circle symbol if it is question
            }
          }))
        .style('fill', (d) => {
          if (!this.showDrops) {
          return d._children ? '#5D87BE' : '#fff';
          }
          
          if (self.showDrops) {
            let childVisits = 0
            if (d.children) {
            for (const child of d.children) {
              childVisits = childVisits + child.data.data.visitors
            }
          }
          else {return '#ff0000';}
          
            let drops = d.data.data.visitors - childVisits
            const color = self.visitorsColor - ((drops / d.data.data.visitors) * self.visitorsColor);
            return '#ff'  + Math.floor(color).toString(16) + Math.floor(color).toString(16);
          }
        });
        // insert visitors text
        nodeEnter.append('text')
        .attr('dx', function() {return -8; })
        .attr('dy', function() {return 3; })
        .attr('display', (d) => {if (this.showDrops === false) {
                                      return 'none';
                                      } else {
                                        return 'initial';
                                      }
                                    })
        .attr('class', 'nodevisitors')
        .style('fill', function() {return '#000'; } )
        .text(function(d) {
          
          if (self.showDrops) {
            let childVisits = 0
            if (d.children) {
            for (const child of d.children) {
              childVisits = childVisits + child.data.data.visitors
            }
          }
            let drops = d.data.data.visitors - childVisits
          if (Math.round((drops / d.data.data.visitors) * 100) > 0 ) {
            return Math.round(drops / d.data.data.visitors * 100) + '%';
          } else {
            return 'N/A';
          }
        }
        return 'N/A';
        });
      
        // insert message text
        nodeEnter.append('foreignObject')
        .attr('width', 185)
        .attr('height', 72)
        .attr('x', -168)
        .attr('y', -75)
        .attr('id', (d) => 'node_butt_' + d.data.id)
        .attr('class', (d) => {
          if (d.data.id !== 0 && d.data.text && d.data.text.length > 0) {
            return 'fobject_' + d.data.id + '_edited';
          } else {
            return 'fobject';
          }
        })
        .append('xhtml:body')
        .attr('class', 'fobj')
        .style('left', '10px')
        .style('border-radious', '10px')
        .html((d) => {
         /* if (d.data.node_label !== null && d.data.node_label.length) {    //toDO label implimentation
            let k = '<div class = "container">';
           return k += '<div class = "edited"><span><xmp>' + d.data.node_label + '</xmp></span></div>';
          } else*/ if (d.data.text &&d.data.text.length > 0) {
            let k = '<div class = "container">';
            let node_names = '';
            if (typeof (d.data.text[0].node_text) === 'object') {
                node_names += '<div class = "edited" ><span><xmp>' + JSON.stringify(d.data.text[0].node_text) + '</xmp></span></div>'; 

            } else {
              for (let j = 0; (j <= d.data.text.length - 1) && (j < 2); j++) {
                node_names = '<div class = "edited" ><span><xmp>' + d.data.text[j].node_text + '</xmp></span></div>' + node_names;
              }
              if (d.data.text &&d.data.text.length > 2) {
                node_names = '<div class = "edited" ><span><xmp>' + '  +  ' + '</xmp></span></div>' + node_names;
              }
            }
            k += node_names;
            return k + '<div id ="node_text_' + d.data.data.id + '_' + this.total_node_text + '" class ="node_text " style="display:none" contentEditable = "true" data-text="Enter text here"></div><div class ="node_text2" style="display:none"><span>+</span></div></div>';
          }

        })
        .on('mouseover', (d:any) => {
        //  this.createNodesInfoBox(d);
      });
     // .on('mouseout', () => { d3.selectAll('.tooltip').remove()});





        node.select('circle.node')
        .attr('r', 4.5)
        .style('fill', function (d) {
          return d._children ? '#5D87BE' : '#fff';
        });

      const nodeUpdate = nodeEnter.merge(node);

      nodeUpdate.transition()
        .duration(0)
        .attr('transform', (d) => {
          return 'translate(' + d.y + ',' + d.x + ')';
        });


        nodeUpdate.select('circle.node')
      .attr('r', 5)
      .style('stroke-width', '2px')
      .style('stroke', 'steelblue')
      .style('fill', (d) => {
        if (d.data.id === 0) {
          return 'red';
        }
        if (d._children) {
          return '#5D87BE';
        }
      })
      .attr('cursor', 'pointer');

    const nodeExit = node.exit().transition()
      .duration(0)
      .style('opacity', 1e-6).remove();



}

plotEdges() {
 let id = 0;
 const self = this;
  const link = this.svg.selectAll('path.link')
      .data(this.links, (d) => ++id);

    const linkArrow = this.svg.append('defs').selectAll('marker')
      .data(this.links)
      .enter()
      .append('marker')
      .attr('id', 'arrowhead')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 15)
      .attr('refY', -0.5)
      .attr('markerWidth', 6)
      .attr('markerHeight', 6)
      .attr('orient', 'auto')
      .style('stroke-width', '6px')
      .style('fill', () => {
            return '#5d87be'; 
          })
      .append('path')
      .attr('d', 'M0,-5L10,0L0,5');

    const linkEnter = link.enter().insert('path', 'g')
      .attr('class', 'link')
      .attr('id', (d) => {
        return `link_${new Date(d.tms).getTime()}`;
      })
      .style('fill', 'none')
      .style('stroke', (d) => {
        if (d.source.data.id[0] === 'F') {
          return '#ff0000';
        } else {

          

          if (this.showTraffic && d.target.data.data.visitors) {

           const maxVisits = self.nodes.find((x) => {
             return x.data.id == '9f3fb353';
           });
           
           const color = this.visitorsColor - ((d.target.data.data.visitors / maxVisits.data.data.visitors) * this.visitorsColor);
           return '#' + Math.floor(color).toString(16) + Math.floor(color).toString(16) + Math.floor(color).toString(16);
          
        }
          return '#5D87BE';
        }
      })
      .style('stroke-width', (d) => {
        const isNodetgt = this.nodes.some(function (el) {
          if (d.target.data) {
            return d.target.data.id === el.data.id;
          }
        });
        const isNodesrc = this.nodes.some(function (el) {
          return d.source.data.id === el.data.id;
        });
        if (isNodetgt === true && isNodesrc === true) {
          return '6px';
        } else {
          return '0px';
        }
      })
      .attr('display', (d) => {
        if (d.target.data.orphan === true) {
          return 'none';
        }
      })
      .attr('d', function (d) {
          return self.graphService.diagonal(d);
      });




}

plotLinkLabels() {
  let i = 0;
  const self = this;
  d3.selectAll('.link-text').remove();
  const linkText = this.svg.selectAll('.link-text')
    .data(this.links);

  linkText.enter().append('foreignObject')   // create object to hold edge label
    .attr('width', 85)
    .attr('height', 50)
    .attr('class', 'link-text')
    .attr('id', (d) => `link-text-${d.source.data.id}-${d.target.data.id}`)
    .style('fill', 'black')
    .style('font-size', '13px')
    .attr('display', (d) => {
      const isNodetgt = this.nodes.some(function (el) {
        if (d.target.data) {
          return d.target.data.id === el.data.id;
        }
      });
      const isNodesrc = this.nodes.some(function (el) {
        return d.source.data.id === el.data.id;
      });
      if (isNodetgt === true && isNodesrc === true) {   // display only if there is a source and a target node
        return 'block';
      } else {
        return 'none';
      }
    })
    .attr('x', function (d) {   // calculate cordinates
        return ((d.source.y + d.target.y) / 2 - 40);
    })
    .attr('y', function (d) {

        return ((d.source.x + d.target.x) / 2);

    })
    .attr('dy', '.35em')
    .append('xhtml:div')
    .append('div')
    .attr('class', 'linkLabel')   // create the label of the link
    .attr('id', (d) => {
      return `linkLabel_${i++}`;
    })
    .text(function (d) {
      if (d.source.data.id[0] === 'F' && d.source.data.data.name.length) {   // put name if it is a functional node
        const label = d.source.data.data.name[0].node_text.fret.find((x) => x.label_id === d.id);
        if (label) {
          return label.text;
        }
      } else {

        const edge = self.edgeData.filter(x => d.source.data.id == x.source && d.target.data.id == x.target);
        return edge[0].label;
      }
    })
    .on('mouseover', (d:any) => {
      //this.createEdgeInfoBox(self, d);
    });





}

createNodesInfoBox(d){
  d3.selectAll('.tooltip').remove();
  const div = d3.select('#tree').select('.group').append('foreignObject')
  .attr('width', 300)
  .attr('height', 105)
  .attr('x', 0 )
  .attr('y', 0 )

  .attr('transform', () => {
    return 'translate(' + (d.y + 10) + ',' + (d.x + 10) + ')';
  })
  .append('xhtml:body')
  .attr('class', 'tooltip')
  .style('opacity', 0);
div.transition()
  .duration(200)
  .style('opacity', 9)
  .style('position', 'absolute');
div.html((c) => {
  if (d.data.text.length > 0) {
    let k = '<div class = "branch-card">';
    let node_names = '';
    if (typeof (d.data.text[0].node_text) === 'object') {
        node_names += '<div class = "nodeinfo"><span >' + JSON.stringify(d.data.text[0].node_text) + '</span></div>'; 

    } else {
      for (let j = 0; j < d.data.text.length; j++) {
        node_names = node_names + '<div class = "nodeinfo"><span>' + d.data.text[j].node_text + '</span></div>';
      }

    }
    k += node_names;

    return k ;
  }
});
}

createEdgeInfoBox(self,d) {
  d3.selectAll('.tooltip').remove();
      const div = d3.select('#tree').select('.group').append('foreignObject')
      .attr('width', 150)
      .attr('height', 105)
      .attr('x', 0 )
       .attr('y', 0 )
     // .attr('dy', '.35em')
      .attr('transform', (c) => {
        return 'translate(' + ((d.source.y + d.target.y) / 2 - 40 + 40) + ',' + ((d.source.x + d.target.x) / 2 + 40) + ')';
      })
      
      .append('xhtml:body')
      .attr('class', 'tooltip')
      .style('opacity', 0);
    div.transition()
      .duration(200)
      .style('opacity', 9)
      .style('position', 'absolute');
    div.html((c) => {
      const edge = self.edgeData.filter(x => d.source.data.id == x.source && d.target.data.id == x.target);
      return '<div class = "branch-card" style="width:149px">' + edge[0].label + '</div>';
    });
  
}

showAnalytics(evt) {
  this.response = false;
  this.showTraffic = evt[0];
  this.showDrops = evt[1];
  this.showTimestamps = evt[2];
  d3.select('#tree').remove();
  this.getNodeData([this.nodeData, []]);
  this.initialise();
  this.plotNodes();
  this.plotEdges();
  this.plotLinkLabels();
  this.response = true;

}

async loadDates(evt) {
  if (evt) {
  d3.select('#tree').remove();
  await this.core.loadPathsTree(this.script, this.branch, evt).subscribe(    // obesrver that receives all the graph data from loopback
    response => {
      this.nodeData = response[0];   // save node data from db
      this.edgeData = response[1];    // save edge data from db
      this.getNodeData([this.nodeData, []]);  // save node data in a heirarchical stracture
      this.initialise();    // initialise the tree
      this.plotNodes();     // plot the tree nodes
      this.plotEdges();     // plot the tree edges
      this.plotLinkLabels();   // plot the labels of the links
      this.response = true;
    }, err => {
      console.log(err);
      alert('No data available');
    });
}
}

}
