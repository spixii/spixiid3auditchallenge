import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// tslint:disable-next-line: max-line-length
import { MatToolbarModule, MatCheckboxModule, MatDialogModule, MatButtonToggleModule, MatSelectModule, MatAutocompleteModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatCardModule, MatMenuModule, MatExpansionModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { TreeComponent } from './tree/tree.component';
import { AnalyticsMainComponent } from './analytics-main/analytics-main.component';
import { AnalyticsRoutingModule } from './analytics-routing.module';
import { NvD3Module } from 'ng2-nvd3';
import {MatTooltipModule} from '@angular/material/tooltip';
import { AnalyticsToolboxComponent } from './analytics-toolbox/analytics-toolbox.component';
import { FormsModule } from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/';


@NgModule({
  imports: [
    CommonModule,
    AnalyticsRoutingModule,
    NvD3Module,
    MatTooltipModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatMenuModule,
    FormsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  declarations: [TreeComponent, AnalyticsMainComponent, AnalyticsToolboxComponent]
})
export class AnalyticsModule { }
