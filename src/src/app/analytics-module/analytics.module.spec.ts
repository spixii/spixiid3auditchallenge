import { AnalyticsModuleModule } from './analytics.module';

describe('AnalyticsModuleModule', () => {
  let analyticsModuleModule: AnalyticsModuleModule;

  beforeEach(() => {
    analyticsModuleModule = new AnalyticsModuleModule();
  });

  it('should create an instance', () => {
    expect(analyticsModuleModule).toBeTruthy();
  });
});
