import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GraphService } from '../graph.service';
import { ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';
import { NodeBoxComponent } from '../node-box/node-box.component';
import { EdgeBoxComponent } from '../edge-box/edge-box.component';
import { SearchPathService } from '../search-path.service';
import { DataService } from '../../core-module/data.service';
import { CoreService } from '../../core-module/core.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';

declare const d3;

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css'],
  encapsulation: ViewEncapsulation.None
})


/**
 * This class handles the whole graph representation and the actions you can take
 * on the graph
 *
 * Graph-service contains utility functions used by this component to perform some of its tasks
 *
 * Graph building proccess
 * -----------------------
 * - List of nodes is converted into a tree format (setNodeData())
 * - Graph space is initialised (initialiseGraph())
 * - Graph is updated (updateChart())
 *    * All nodes are plotted (plotNodes())
 *    * All edges are plotted (plotEdges())
 *    * All coupled edges are plotted (plotCouplingEdges())
 *    * All normal edges link labels are updated (updateLinkText())
 *    * All coupling edges link labels are update (updateCouplingLinkTexy())
 * Two main click events
 * ------------------------
 * - openNodeDialog
 * - openEdgeDialog
 *
 * Other importand functions
 * -------------------------
 * - dragStart
 * - DragEnd
 * Handle the situation where a user drags a line to create a new edge
 *
 * Optional
 * ------------
 * Please feel free to change any comment or description you beleive
 * is not please precise or well undestandable
 *
 */
export class GraphComponent implements OnInit {
  historyObject = {
    node: {
      id: '',
      operation: '',
      data: '',
    },
    path: {
      id: '',
      operation: '',
      data: '',
      pathtype: ''
    }
  };

  margin: any;  // margin of the graph
  width: number; // width of the graph
  height: number; // height of the graph
  currentUrl;   // the url of the grapg
  svg: any;     // svg used by d3
  duration: number;  // duration of d3 transition
  root: any;          // the root of the conversation
  tree: any;   // the conversation as a tree
  treeData: any = [];  // all nodes into a tree format
  nodes: any;    // all the nodes
  links: any;     // all the edges on the graph stracture
  selected;    // the selected object
  id = 0;
  keep;
  nMode = false;    // specifies if a new node is created
  deletedNodes = [];   // all deteted nodes
  functions = [];   // all the functional nodes
  isHTML = RegExp.prototype.test.bind(/(<([^>]+)>)/i);
  currEdge;    // possibly not used
  adNod = true;    // if node type is selected on the bar
  dbmode = true;  // edges are just taken from db
  typeNode = 'Q';     // the type of the node that is selected on the bar
  spinner = '#6576BF';    // the color of the loading spinner
  response = false;
  extractDashed = false;   // possibly not used
  collapse;        // if children are collapsed
  addednodes;    // the number of added nodes
  coords;
  coupling = [];  // array of coupling edges
  couplingEdges = 0;  // number of coupling edges
  editLabelMode = true;   // you are allowd to edit labels
  addNodeMode = true;     // new node can be added
  addEdgeMode = false;    // new edge can be added
  dragStartPos;    // the node dragging new edge has started from
  total_node_text = 0;   // total number node texts
  branch;     // the current branch id
  allPath;  // edges taken from db
  childs = [];
  child_data = [];
  allNodes;  // nodes taken from db
  createOrphanEdge = false;   // possibly not used
  circle;    // possibly not used
  currentNode;  // the current not that is clicked
  dragEndPos = null;   // the node dragging new edge ends to
  path = [];   // array of all the edges
  addedEdges = 0;   // number of edges
  edgeDialogueMode = false;    // weather an edge dialog is open
  message;   // possibly not used
  multipleEdge;    //  possibly not used
  deletedEdges = [];    // all the deleted edges
  searchPaths = [];    // the edges the user searched for
  script;     // the current script
  url;       // the current url
  variables;
  
  entry = { ENTRY_POINT: 'Q1' };  // the root node on the beggining of the conversation
  updatedGraphData = true;   // weather the graph is updated
  @Output() collapseNodes = new EventEmitter();
  @Output() nodeCreated = new EventEmitter();     // event triggered when a new node is created (transfers the data)
  @Output() edgeCreated = new EventEmitter();     // event triggered when a new edge is created
  constructor(
    public dialog: MatDialog,
    public graphService: GraphService,
    private route: ActivatedRoute,
    public data: DataService,
    public searchpath: SearchPathService,
    public core: CoreService,
    private router: Router
  ) {
    this.id = 0;
  }

  ngOnInit() {
    this.currentUrl = this.router.url;
    this.script = this.route.snapshot.paramMap.get('script_id');     // get the script id from url
    this.branch = this.route.snapshot.paramMap.get('branch_id');     // get the branch id from url
    this.url = this.route.snapshot.pathFromRoot.pop().url.map(u => u.path).join('/');
    this.loadData();


  }

  /**
   * Function that is used to fetch data from db and initializes graph
   */
  async loadData() {

    await this.core.loadData(this.script, this.branch).subscribe(    // obesrver that receives all the graph data from loopback
      response => {
        this.response = true;
        this.allPath = response[1];  // all the edges
        this.allNodes = response[0]; // all the nodes
        this.functions = response[4]; // all the functional nodes
        this.data.setNodeValue(response[0]);  // save all the data of the nodes
        this.setTimeout(response[2]);
        const nodeArray = this.graphService.attachToMainParent(response[0], this.entry);  // configures the table of nodes
        this.setNodeData([nodeArray, []]);  // convert the array of nodes into a tree stracture
        this.initialiseGraph();   // initialise the graph
        this.updateChart(this.root);   // update and plot the graph
       // this.plotCouplingEdges();
       // this.updateLinkText();        // update the text on the edges
        //this.updateCouplingText();
      
      }, err => console.log(err));
  }

 /**
  * Function that sets the node data and edges
  * @param data All the data of the nodes coming from the database
  */
 async setNodeData(data) {
  const nodeData = data[0];  // the array of nodes
  const self = this;
  if (nodeData.length !== 0) {
    this.treeData = d3.stratify()   // create the graph data by taking the node id and the parent id
      .id(function (d) { return d.id; })
      .parentId(function (d) {
        if (d.parent) {
          return d.parent;
        }
      })
      (nodeData);
    // assign the name, param and label to each node
    this.treeData.each(function (d) {
      d.name = d.data.name;
      d.node_param = d.data.node_param;
      d.node_label = d.data.node_label;
    });
  }


}
/**
 * function that initialises and load the graph
 */
initialiseGraph() {

  const self = this;
  this.margin = { top: 39, right: 90, bottom: 30, left: 32 };
  this.width = 500 - this.margin.left - this.margin.right;
  this.height = 400 - this.margin.top - this.margin.bottom;
  const zoomed = () => self.svg.attr('transform', d3.event.transform);
  const zoom = d3.zoom().scaleExtent([0.1, 2]).on('zoom', zoomed);
  this.svg = d3.select('#graphcontainer').append('svg')   // create the svg of the whole graph
    .attr('id', 'graph')
    .attr('width', '90%')
    .attr('height', '90%')
    .on('click', () => {    // to understand better
       this.clickOnEmpty(self);
      self.dragEndPos = null;
      self.dragStartPos = null;
      d3.select('#graph').on('dblclick.zoom', null);
    })
    .on('mouseover', (d) => {
      self.coords = d3.mouse(d3.event.currentTarget);
      d3.select('#graph').call(zoom);
      self.addNodeMode = false;
    })
    .on('contextmenu', function (d, i) {
      self.dragStartPos = null;
      self.dragEndPos = null;
    })
    .append('g')   // create the class containg the graph
    .attr('class', 'group')
    .attr('id', 'g-main')
    .attr('transform', 'translate(150, 300)');

  this.duration = 0;
  this.root = d3.hierarchy(this.treeData, (d) => d.children);  // create a tree
  this.root.x0 = this.height / 2;
  this.root.y0 = this.width / 10;

}

/**
 * Function for updating the existing chart and reloading the dat
 * @param source The source of the conversation tree
 * @param updatetype [Optional] The type of the update operation
 */
async updateChart(source, updatetype?) {
  if (source !== undefined) {
    this.tree = d3.tree()   // create the conversation tree
      .size([this.height, this.width])
      .nodeSize([30, 0])   // the size of each node
      .separation(function separation(a, b) { return 12; });  // the space between nodes
    const self = this;
    let id = 0;
    this.treeData = this.tree(this.root);
    this.nodes = this.treeData.descendants();   // get nodes from tree
    this.links = this.tree(this.treeData).links();  // get edges from tree
    if (this.deletedNodes.length) {
      for (const nod of this.deletedNodes) {
        this.nodes = this.nodes.filter((d) => {
          if (d.data.id === nod) {
            this.path = this.path.filter((x) => x.source.data.id !== nod);
            this.path = this.path.filter((x) => x.target.data.id !== nod);
            this.coupling = this.coupling.filter((x) => x.target.data.id !== nod);
            this.coupling = this.coupling.filter((x) => x.source.data.id !== nod);
            if (d.children && d.children.length > 0) {
            for (const child of d.children) {
              const hasParent = this.path.filter((x) => x.target.data.id === child.data.id).length;
              const hasCoupling = this.coupling.filter((x) => x.target.data.id === child.data.id).length;
              if (hasParent + hasCoupling === 0) {
              
                if (child.data.data) {
                child.data.data.orphan = true;
                }
                else {
                  child.data.orphan = true;
                }
                child.parent = d.parent;
              
               }
            }
          }

            return false;
          }
          return true;
        });
      }
    }
    //  give the functional edges their label
    for (const way of this.allPath) {  // for each edge
      if (way.source[0] === 'F') {
        const n = this.nodes.find((x) => x.data.id === way.source);  // get the source node
        if (n) {
          if (n.data.name[0]) {
            const fun = this.functions.find((x) => x.fname === n.data.name[0].node_text.fval);
            if (fun) {
              const lab = fun.fret.find((x) => x.label_id === way.id);
              if (lab) {
                way.label = lab.text;
              }
            }
          }
        }
      }
    }
    // put any new paths into the data
    if (this.dbmode) {
      for (const path of await this.allPath) {
        let k = 0;
        const Edges = [];
        for (const lin of this.links) {
          if (path.source === lin.source.data.id.toString() && path['target'] === lin.target.data.id.toString()) {
            k++;
            path['source'] = lin.source;
            path['target'] = lin.target;
            if (this.path.findIndex((x) => (x.source.data.id === path.source.data.id &&
              x.target.data.id === path['target']['data']['id'] && x.pathCount === path.pathCount)) > -1) {
                this.coupling.push(path);     // if coupling edge, push into coupling array
            } else {
               this.path.push(path);        // if normal, push into normal array
            }
          }
        }
             if (k === 0) {
             if (path.source) {
               this.coupling.push(path);
             }
             await this.coupling.filter((thing, index, sel) =>
               index === sel.findIndex((t) => (
                 t.source === thing.source && t['target'] === thing.target
               ))
             );
             await this.coupling.sort(function (a, b) {
               return new Date(a.tms).getTime() - new Date(b.tms).getTime();
             });
           }

      }
    }

    this.dbmode = false;
     await this.coupling.filter(a => {
         return a.source !== undefined;
       });
    this.addedEdges = this.path.length;
    this.plotNodes(source, id, updatetype);
    this.plotEdges();
    await this.plotCouplingEdges();
    await this.updateLinkText();
    await this.updateCouplingText();
  }
}

 /**
  *  Function used to update the edge labels when reloading the graph
  */
 updateLinkText() {
  d3.selectAll('.link-text').remove();  // remove all current text from edges
  for (const p of this.path) {
    if (p.source.data === undefined) {
      const src = this.nodes.filter(x => x.data.id === p.source);   // if no data get the source of the edge
      const tgt = this.nodes.filter(x => x.data.id === p.target);   // if no data get the target of the edge
      p.source = src[0];
      p.target = tgt[0];
    }
  }
  this.path = this.path.filter((cou: any, index: any, sel: any) =>
    index === sel.findIndex((t: any) => (
      t.source.data.id === cou.source.data.id && t['target']['data']['id'] === cou.target.data.id
    ))
  );
  const self = this;
  d3.selectAll('.link-text').remove();
  const linkText = this.svg.selectAll('.link-text')
    .data(this.path);

  linkText.enter().append('foreignObject')   // create object to hold edge label
    .attr('width', 85)
    .attr('height', 50)
    .attr('class', 'link-text')
    .attr('id', (d) => `link-text-${d.source.data.id}-${d.target.data.id}`)
    .style('fill', 'black')
    .style('font-size', '13px')
    .attr('display', (d) => {
      const isNodetgt = this.nodes.some(function (el) {
        if (d.target.data) {
          return d.target.data.id === el.data.id;
        }
      });
      const isNodesrc = this.nodes.some(function (el) {
        return d.source.data.id === el.data.id;
      });
      if (isNodetgt === true && isNodesrc === true) {   // display only if there is a source and a target node
        return 'block';
      } else {
        return 'none';
      }
    })
    .attr('x', function (d) {   // calculate cordinates
      const lnk = self.links.find((x) => {
        if (x['source'] !== null) {
          if (x['source']['data']['id'] === d.source.data.id && x.target.data.id === d.target.data.id) {
            return x;
          }
        }
      });
      if (lnk) {
        return ((lnk.source.y + lnk.target.y) / 2 - 40);
      }
    })
    .attr('y', function (d) {
      const lnk = self.links.find((x) => {
        if (x['source'] !== null) {
          if (x['source']['data']['id'] === d.source.data.id && x.target.data.id === d.target.data.id) {
            return x;
          }
        }
      });
      if (lnk) {
        return ((lnk.source.x + lnk.target.x) / 2);
      }
    })
    .attr('dy', '.35em')
    .append('xhtml:div')
    .append('div')
    .attr('class', 'linkLabel')   // create the label of the link
    .attr('id', (d) => {
      return `linkLabel_${new Date(d.tms).getTime()}`;
    })
    .attr('contentEditable', (d) => {
      if (self.path[d.id - 1] !== undefined) {
        self.svg.on('.zoom', null);
        return self.path[d.id - 1].edgetype === 'button' ? true : false;
      }
    })
    .text(function (d) {
      if (d.source.data.id[0] === 'F' && d.source.data.data.name.length) {   // put name if it is a functional node
        const label = d.source.data.data.name[0].node_text.fret.find((x) => x.label_id === d.id);
        if (label) {
          return label.text;
        }
      } else {
        const link = self.path.filter(x => d.source.data.id === x.source.data.id && d.target.data.id === x.target.data.id);
        if (link[0] !== undefined) {
          if (link[0].edgetype !== 'button') {
            return link[0].edgetype;
          } else {
            if (link[0]) {
              return link[0].label;
            }
          }
        }
      }
    })
    .on('click', (d) => {     // on click of a label
      d3.event.preventDefault();
      self.dragStartPos = null;
      self.addNodeMode = false;
      setTimeout(function () {
        self.addNodeMode = true;
      }, 2000);
      const edge = this.path.find(x => x.source.data.id === d.source.data.id &&
        x.target.data.id === d.target.data.id);
      this.openEdgeDialog('existing_edge', edge);
      d3.event.stopPropagation();
    }
    )
    .on('mouseover', (d) => {
      self.highlightNodes(d);

    })
    .on('mouseout', (d) => {
      self.updateChart(this.root);

    });

  linkText.transition()
    .attr('x', null)
    .attr('y', null)
    .attr('transform', function (d) {
      return 'translate(' + ((d.source.y + d.target.y) / 2 - 30) + ',' + ((d.source.x + d.target.x) / 2 - 9) + ')';
    });
  linkText.exit().transition()
    .remove();
}
/**
 * function that plots all the nodes on the graph
 * @param source The source of the conversation tree
 * @param id The id of the first node
 * @param sourcetype The type of the operation
 */
plotNodes(source, id, updatetype?) {
  const self = this;
  const node = this.svg.selectAll('g.node')  // create class containing the nodes
      .data(this.nodes, (d) => ++id);    // insert node data into the graph data
    this.nodes.forEach((d) => {   // set the cordinates of each node
      d.x0 = d.x;
      d.y0 = d.y;
    });
    // all the node objects created
    const nodeEnter = node.enter().append('g')
      .attr('class', 'node')    // create node object
      .attr('id', (d) => d.data.id)  // set its id
      .style('display', (d) => {
        if (d.data.id === 'Q') {
          return 'none';
        }
      })
      .on('click', (d) => {
        self.currentNode = d;
        self.addNodeMode = false;
        setTimeout(function () {
          self.addNodeMode = true;
        }, 1000);
      });

    if (updatetype !== 'drag') {
      this.nodes.forEach((d) => { d.y = d.y0 + (d.depth * 180) - (d.height * 22); d.x = (d.x0 / 2.8 + 45); });
    }

    if (source.data.id) {
      nodeEnter.append('path') // set the edge color
        .style('stroke', function (d) {
          if (d.data.id && d.data.id[0] === 'Q') { return '#5D87BE'; } else
            if (d.data.id && d.data.id[0] === 'F') { return 'red'; } else {
              return 'green';
            }

        })
        .attr('class', 'nodesymbol')  // create the circle represeting the node
        .attr('id', (d) => `node_${d.data.id}`)
        .style('fill', function (d) { return '#fff'; })
        .attr('d', d3.symbol()
          .size( (d) => {
              return 100;
              })
          .type(function (d) {
            {
              if (d.data.id && d.data.id[0] === 'Q') { return d3.symbolCircle; } else     // set circle symbol if it is question
                if (d.data.id && d.data.id[0] === 'F') { return d3.symbolTriangle; } else { // set triangle symbol
                  return d3.symbolStar;                                                     // if it is functional node
                }
            }
          }))
        .style('fill', (d) => {
          return d._children ? '#5D87BE' : '#fff';
        })
        .on('click', (d) => {       // when a node icon is clicked
          if (d) {
            self.clusterNodes(d, '');  // collapse nodes
           
          } else {
          }
        })
        .on('mousedown.sp', (d) => {
          d3.select('#graph').on('mousedown.zoom', null);
          this.selected = d;
          if (d !== undefined) {
            this.addEdgeMode = false;
            this.drawLine(d);
            this.dragStart(d);
            self.svg.on('.zoom', null);
          }
        })
        .on('mouseup', (d) => {
          if (d !== undefined) {
            this.historyObject.node.operation = 'connection';
            this.historyObject.path.operation = 'connection';
            this.dragEnd(d);
          }
        })
        .on('mouseover', (d) => {
          self.highlightLinks(d);
        })
        .on('mouseout', (d) => {
           self.plotEdges();
          self.plotCouplingEdges();
        });


    }
    // create object after each node
    nodeEnter.append('foreignObject')
      .attr('width', 185)
      .attr('height', 72)
      .attr('x', -168)
      .attr('y', -75)
      .attr('id', (d) => 'node_butt_' + d.data.id)
      .attr('class', (d) => {
        if (d.data.id !== 0 && d.data.name.length > 0) {
          return 'fobject_' + d.data.id + '_edited';
        } else {
          return 'fobject';
        }
      })
      .append('xhtml:body')
      .attr('class', 'fobj')
      .style('left', '10px')
      .style('border-radious', '10px')
      .html((d) => {
        // create label and put the node name
        if (d.data.hasOwnProperty('data')) {
          if (d.data.data.node_label !== null && d.data.data.node_label.length) {
            let k = '<div class = "container">';
            k += '<div class = "edited"><span><xmp>' + d.data.node_label + '</xmp></span></div>';
            return k += '<div id ="node_text_' + d.data.data.id + '_' + this.total_node_text + '" class ="node_text " style="display:none" contentEditable = "true" data-text="Enter text here"></div><div class ="node_text2" style="display:none"><span>+</span></div></div>';
          } else if (d.data.data.name.length > 0) {
            let k = '<div class = "container">';
            let node_names = '';
            if (typeof (d.data.data.name[0].node_text) === 'object') {
              if (d.data.data.id[0] === 'F') {
                node_names += '<div class = "edited"><span><xmp>' + JSON.stringify(d.data.data.name[0].node_text.fval) + '</xmp></span></div>';

              } else {
                node_names += '<div class = "edited"><span><xmp>' + JSON.stringify(d.data.data.name[0].node_text) + '</xmp></span></div>';
              }
            } else {
              for (let j = 0; (j <= d.data.name.length - 1) && (j < 2); j++) {
                node_names = '<div class = "edited"><span><xmp>' + d.data.data.name[j].node_text + '</xmp></span></div>' + node_names;
              }
              if (d.data.name.length > 2) {
                node_names = '<div class = "edited"><span><xmp>' + '  +  ' + '</xmp></span></div>' + node_names;
              }
            }
            k += node_names;
            return k + '<div id ="node_text_' + d.data.data.id + '_' + this.total_node_text + '" class ="node_text " style="display:none" contentEditable = "true" data-text="Enter text here"></div><div class ="node_text2" style="display:none"><span>+</span></div></div>';
          } else {
            return '<div class = "node_text new_node_text">Add...</div>';
          }
        } else {
          if (d.data.node_label !== null && d.data.node_label.length) {
            let k = '<div class = "container">';
            k += '<div class = "edited"><span><xmp>' + d.data.node_label + '</xmp></span></div>';
            return k += '<div id ="node_text_' + d.data.id + '_' + this.total_node_text + '" class ="node_text " style="display:none" contentEditable = "true" data-text="Enter text here"></div><div class ="node_text2" style="display:none"><span>+</span></div></div>';
          } else if (d.data.name.length > 0) {
            let k = '<div class = "container">';
            let node_names = '';
            if (typeof (d.data.name[0].node_text) === 'object') {
              if (d.data.id[0] === 'F') {
                node_names += '<div class = "edited"><span><xmp>' + JSON.stringify(d.data.name[0].node_text.fval) + '</xmp></span></div>';

              } else {
                node_names += '<div class = "edited"><span><xmp>' + JSON.stringify(d.data.name[0].node_text) + '</xmp></span></div>';
              }
            } else {
              for (let j = 0; (j <= d.data.name.length - 1) && (j < 2); j++) {
                node_names = '<div class = "edited"><span><xmp>' + d.data.name[j].node_text + '</xmp></span></div>' + node_names;
              }
              if (d.data.name.length > 2) {
                node_names = '<div class = "edited"><span><xmp>' + '  +  ' + '</xmp></span></div>' + node_names;
              }
            }
            k += node_names;
            return k += '<div id ="node_text_' + d.data.id + '_' + this.total_node_text + '" class ="node_text " style="display:none" contentEditable = "true" data-text="Enter text here"></div><div class ="node_text2" style="display:none"><span>+</span></div></div>';
          } else if (d.data.name.length === 0) {
            return '<div class = "node_text new_node_text">Add...</div>';
          }
        }
      }
      )
      .attr('dy', '.35em')
      .attr('text-anchor', function (d) { return d.children || d._children ? 'end' : 'start'; });

    nodeEnter.select('foreignObject')
      .on('click', (d) => {
        d3.event.stopPropagation();
        this.addNodeMode = false;
        this.addEdgeMode = false;
        if (d.data.id[0] !== 'F') {
          this.openNodeDialog(d.data.id);  // open node dialog if a node label is clicked
        } else {
          this.edgeCreated.emit([d, this.functions, this.nodes, 'other']); // emit that a functional code is created
        }
        d3.event.preventDefault();

      });

    node.select('circle.node')
      .attr('r', 4.5)
      .style('fill', function (d) {
        return d._children ? '#5D87BE' : '#fff';
      });

    const nodeUpdate = nodeEnter.merge(node);

    nodeUpdate.transition()
      .duration(this.duration)
      .attr('transform', (d) => {
        return 'translate(' + d.y + ',' + d.x + ')';
      });

    nodeUpdate.select('circle.node')
      .attr('r', 5)
      .style('stroke-width', '2px')
      .style('stroke', 'steelblue')
      .style('fill', (d) => {
        if (d.data.id === 0) {
          return 'red';
        }
        if (d._children) {
          return '#5D87BE';
        }
      })
      .attr('cursor', 'pointer');

    const nodeExit = node.exit().transition()
      .duration(this.duration)
      .style('opacity', 1e-6).remove();
    if (this.deletedEdges.length) {
      for (const edge of this.deletedEdges) {
        this.path = this.path.filter((d) => {
          if (edge !== undefined && d.tms === edge.tms) {
            return false;
          }
          return true;
        });
      }
    }
    let i = 0;
    for (let path of this.path) {
      const lnk = this.links.find((x) => {
        if (x['source'] !== null) {
          if (x['source']['data']['id'] === path.source.data.id && x.target.data.id === path['target']['data']['id']) {
            return x;
          }
        }
      });
      i++;
      path = lnk;
    }
}
/**
 * function that plots all the the edges on the graph
 */
plotEdges() {
  const self = this;
  const link = this.svg.selectAll('path.link')
      .data(this.path, (d) => d.id);

    const linkArrow = this.svg.append('defs').selectAll('marker')
      .data(this.path)
      .enter()
      .append('marker')
      .attr('id', 'arrowhead')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 15)
      .attr('refY', -0.5)
      .attr('markerWidth', 6)
      .attr('markerHeight', 6)
      .attr('orient', 'auto')
      .style('stroke-width', '1px')
      .style('fill', () => {
            return '#5d87be';
          })
      .append('path')
      .attr('d', 'M0,-5L10,0L0,5');

    

    const linkEnter = link.enter().insert('path', 'g')
      .attr('class', 'link')
      .attr('id', (d) => {
        return `link_${new Date(d.tms).getTime()}`;
      })
      .attr('marker-end', 'url(#arrowhead)')
      .style('fill', 'none')
      .style('stroke', '#ccc')
      .style('stroke-width', (d) => {
        const isNodetgt = this.nodes.some(function (el) {
          if (d.target.data) {
            return d.target.data.id === el.data.id;
          }
        });
        const isNodesrc = this.nodes.some(function (el) {
          return d.source.data.id === el.data.id;
        });
        if (isNodetgt === true && isNodesrc === true) {
          return '1px';
        } else {
          return '0px';
        }
      })
      .attr('display', (d) => {
        if (d.target.data.orphan === true) {
          return 'none';
        }
      })
      .attr('d', function (d) {
        const lnk = self.links.find((x) => {
          if (x['source'] !== null) {
            if (x['source']['data']['id'] === d.source.data.id && x.target.data.id === d.target.data.id) {
              return x;
            }
          }
        });

        if (lnk) {
          return self.graphService.diagonal(lnk);
        }

      });
      

    const linkUpdate = linkEnter.merge(link);

    linkUpdate.transition()
      .duration(this.duration)
      .attr('d', (d) => {
        const lnk = self.links.find((x) => {
          if (x['source'] !== null) {
            if (x['source']['data']['id'] === d.source.data.id && x.target.data.id === d.target.data.id) {
              return x;
            }
          }
        });
        if (lnk) {
          return self.graphService.diagonal(lnk);
        }
      })
      .style('stroke', (d) => {
        if (d.source.data.id[0] === 'F') {
          return '#ff0000';
        } else {

          const lnk = self.links.find((x) => {
            if (x['source'] !== null) {
              if (x['source']['data']['id'] === d.source.data.id && x.target.data.id === d.target.data.id) {
                return x;
              }
            }
          });
          return '#5D87BE';
        }
      })
      .style('stroke-width', (d) => {
        const isNodetgt = this.nodes.some(function (el) {
          if (d.target.data) {
            return d.target.data.id === el.data.id;
          }
        });
        const isNodesrc = this.nodes.some(function (el) {
          return d.source.data.id === el.data.id;
        });
        if (isNodetgt === true && isNodesrc === true) {
          return '1px';
        } else {
          return '0px';
        }
      });

    const linkExit = link.exit().transition()
      .duration(this.duration)
      .attr('d', function (d) {
        const lnk = self.links.find((x) => {
          if (x['source'] !== null) {
            if (x['source']['data']['id'] === d.source.data.id && x.target.data.id === d.target.data.id) {
              return x;
            }
          }
        });
        if (lnk) {
          return self.graphService.diagonal(lnk);
        }
      })
      .remove();
}
  /**
   * function that executes actions when a user clicks anywhere on the graph space
   * @param self Reference the GraphComponent class
   */
  async clickOnEmpty(self) {
    if (self.adNod === true) {
      const mouseClickPoint = d3.mouse(d3.event.currentTarget);

      if (self.dragStartPos === undefined || self.dragStartPos === null) {
        const distancearray = self.graphService.getdistances(mouseClickPoint, this.nodes);
        if (distancearray.length) {
          const selectedIndex = distancearray.indexOf(Math.min(...distancearray));
          self.selected = self.nodes[selectedIndex];
          self.dragStartPos = self.nodes[selectedIndex];
          self.nMode = true;
        } else {
          self.selected = self.nodes[self.nodes.length - 1];
          self.dragStartPos = self.nodes[self.nodes.length - 1];
          self.nMode = true;
        }
        self.openNodeDialog(`${self.typeNode}${self.addednodes + 1}`);  // if user clicks anywhere open node dialog box
        self.dragStartPos = null;
      } else if (self.dragEndPos === undefined || self.dragEndPos === null) {  // when user draws a line from
        try {                                                                  // a start node but not on a target
          this.historyObject.node.operation = 'create';
          this.historyObject.path.operation = 'create';
        } catch {
          console.error('error creating hostoy object');
        } finally {

          let id_index = self.nodes.findIndex(x => x.data.id === `${self.typeNode}${self.addednodes}`);
          self.addednodes = self.nodes.reduce(function (max, obj) {
            return +obj.data.id.replace(/\D/g, '') > +max.data.id.replace(/\D/g, '') ? obj : max;
          });
          self.addednodes = +self.addednodes.data.id.replace(/\D/g, '') + 1;

          d3.select(`#node_butt_${this.typeNode}${this.addednodes}`).select('body').style('display', 'none');

          const newNodeObj = {
            id: `${self.typeNode}${self.addednodes}`,
            name: [],
            type: 'new',
            node_param: null,
            node_label: null,
            attributes: [],
            children: []
          };

          this.historyObject.node.id = `${self.typeNode}${self.addednodes}`;

          const newNode = d3.hierarchy(newNodeObj);

          if (self.dragStartPos.children === undefined || !self.dragStartPos['children'].length) {
            self.dragStartPos['children'] = [];
            self.dragStartPos.data['children'] = [];
          }
          self.dragStartPos.children.push(newNode);
          self.dragStartPos.data.children.push(newNode.data);
          self.tree = d3.tree()
            .size([self.height, self.width])
            .nodeSize([30, 0])
            .separation(function separation(a, b) { return 12; });
          self.treeData = self.tree(self.root);
          self.nodes = self.treeData.descendants();

          d3.event.preventDefault();
          d3.select('#graph').on('mouseup', self.dragEnd(newNode, 'nonode'));
          d3.event.stopPropagation();

          id_index = self.addednodes + 1;
          self.updateChart(self.root, 'drag');
         // this.plotCouplingEdges();
         // self.updateLinkText();
         // this.updateCouplingText();

        }

      }
    }
  }

   /**
   * Function that creates a coupling edge when the user draws a line
   * from two unrealated nodes(nodes without having any relations)
   */
   async plotCouplingEdges() {
     d3.selectAll('.couplinglink').remove();
     if (this.coupling.length > 0) {
       for (const c of await this.coupling) {
         if (c.source.data === undefined) {
           const src = this.nodes.filter(x => x.data.id === c.source);
           const tgt = this.nodes.filter(x => x.data.id === c.target);
           if (src[0] && tgt[0] !== undefined) {
             c.source = src[0];
             c.target = tgt[0];
           }
         }
       }
       if (this.deletedEdges.length) {
         for (const edge of this.deletedEdges) {
           this.coupling = this.coupling.filter((d) => {
             if (d.tms === edge.tms) {
               return false;
             }
             return true;
           });
         }
       }
       this.coupling = await this.coupling.filter(obj => {
         return typeof (obj.source) !== 'string';
       });
       for (const path of this.coupling) {
         let p = 0;
         for (const inner of this.coupling) {
           if (((path.source.data.id === inner.source.data.id) && (path['target']['data']['id'] === inner.target.data.id)) ||
             ((path.source.data.id === inner.target.data.id) && (path['target']['data']['id'] === inner.source.data.id))) {
             p++;
             inner.pathCount = p;
           }
         }
       }

       if (this.coupling.length > 0) {
         const self = this;
         d3.selectAll('.couplinglink').remove();
         const link = await this.svg.selectAll('path.couplinglink')
           .data(await this.coupling, (d) => (d.id = ++self.couplingEdges));
         const linkArrow = await this.svg.append('defs').selectAll('markerCoupling')
           .data(await this.coupling)
           .enter()
           .append('markerCoupling')
           .attr('id', 'arrowhead')
           .attr('viewBox', '0 -5 10 10')
           .attr('refX', 15)
           .attr('refY', -0.5)
           .attr('markerWidth', 6)
           .attr('markerHeight', 6)
           .attr('orient', 'auto')
           .style('stroke-width', '1px')
           .style('fill', '#5d87be')
           .append('path')
           .attr('d', 'M0,-5L10,0L0,5');

         const linkEnter = await link.enter().insert('path', 'g')
           .attr('id', (d) => 'couplinglink_' + new Date(d.tms).getTime())
           .attr('class', 'couplinglink')
           .style('fill', 'none')
           .style('stroke', (d) => {
            if (d.source.data.id[0] === 'F') {
              return '#ff0000';
            } 
            else{
            return 'steelblue'
            }
          })
           .style('stroke-width', '1px')
           //.style('stroke-dasharray', ('3, 3'))
           .attr('marker-end', 'url(#arrowhead)')
           .style('stroke-width', (d) => {
             const isNodetgt = this.nodes.some(function (el) {
               if (d.target.data) {
                 return d.target.data.id === el.data.id;
               }
             });
             const isNodesrc = this.nodes.some(function (el) {
               return d.source.data.id === el.data.id;
             });
             if (isNodetgt === true && isNodesrc === true) {
               return '1px';
             } else {
               return '0px';
             }
           })
           .attr('d', function (d) {
             if (self.createOrphanEdge === false) {
               const source = self.nodes.find((x) => x.data.id === d.source.data.id);
               const target = self.nodes.find((x) => x.data.id === d.target.data.id);
               const o = { source: source, target: target, pathCount: d.pathCount };
               if (source && target) {
                 return self.graphService.dashedDiagonal(o);
               }
             }
           })
           .on('click', (d) => {
             d3.event.preventDefault();
             d.clicked = true;
             self.addNodeMode = false;
             const edge = this.coupling.find(x => x.source.data.id === d.source.data.id
               && x.target.data.id === d.target.data.id && x.pathCount === d.pathCount);
             self.openEdgeDialog('coupling_edge', d);
             d3.event.stopPropagation();
             setTimeout(function () {
               self.addNodeMode = true;
             }, 2000);
           });

       }
     }
   }

  /**
   * Function that updates the Coupling texts when user creates a coupling edge or dashed edge
   * */
   async updateCouplingText() {
     const self = this;
     d3.selectAll('.coupling-text').remove();
     const linkText = this.svg.selectAll('.coupling-text')
       .data(await this.coupling, (d) => (d.id = ++self.couplingEdges));
     await linkText.enter().append('foreignObject')
       .attr('width', 85)
       .attr('height', 14)
       .attr('class', 'coupling-text')
       .style('fill', 'black')
       .style('font-size', '13px')
       .on('click', (d) => {
         d3.event.preventDefault();
         self.addNodeMode = false;
         const edge = this.coupling.find(x => x.source.data.id === d.source.data.id
           && x.target.data.id === d.target.data.id && x.pathCount === d.pathCount);
         self.openEdgeDialog('coupling_edge', edge);
         d3.event.stopPropagation();
         setTimeout(function () {
           self.addNodeMode = true;
         }, 2000);

       })

       .attr('x', function (d) {
         let p, c;
         if (d.source.y !== d.target.y) {
           p = 3 * d.pathCount;
           c = 0;
         } else {
           p = -25 * ((d.pathCount - 1) ? -1 : 1);
           c = 0;
         }
         const source = self.nodes.find((x) => x.data.id === d.source.data.id);
         const target = self.nodes.find((x) => x.data.id === d.target.data.id);
         const o = { source: source, target: target, pathCount: d.pathCount };
         if (source && target) {
           return (((o.source.y + p) + o.target.y) / 2 - 30);
         }
       })
       .attr('y', function (d) {
         let c, p, distance;
         c = (53 * (d.pathCount))
         const source = self.nodes.find((x) => x.data.id === d.source.data.id);
         const target = self.nodes.find((x) => x.data.id === d.target.data.id);
         const o = { source: source, target: target, pathCount: d.pathCount };
         if (source && target) {
           distance = ((source.x) + target.x) / 2;

           if (Math.abs(d.source.x) === Math.abs(+d.target.x)) {
             c = 53 * d.pathCount;
           }
         } else {
           distance = ((d.source.x) + d.target.x) / 2;
         }
         return (distance + c);
       })
       .attr('dy', '.35em')
       .append('xhtml:div')
       .append('div')
       .attr('class', 'linkLabel')
       .style('display', (d) => {
         const isNodetgt = this.nodes.some(function (el) {
           return d.target.data.id === el.data.id;
         });
         const isNodesrc = this.nodes.some(function (el) {
           return d.source.data.id === el.data.id;
         });
         if (isNodetgt === true && isNodesrc === true) {
           return 'block';
         } else {
           return 'none';
         }
       })
       .attr('id', (d) => {
         return `linkLabel_${new Date(d.tms).getTime()}`;;
       })
       .text(function (d) {
         if (d.edgetype !== 'button') {
           return d.edgetype;
         } else {
           if (1) {
             return d.label;
           }
         }
       })
       .on('keyup', (d) => {
         if (d !== undefined) {
           d.label = document.querySelector('#linkLabel' + d.id).innerHTML;
           self.editLabelMode = false;
           setTimeout(function () {
             self.editLabelMode = true;
           }, 2000);
         }
       })
       .on('click', () => {
         self.dragStartPos = null;
         self.addNodeMode = false;
         setTimeout(function () {
           self.addNodeMode = true;
         }, 2000);
       })
       .on('mouseover', (d) => {
        self.highlightNodes(d);
  
      })
      .on('mouseout', (d) => {
        self.updateChart(this.root);
      });
     linkText.transition()
       .attr('x', null)
       .attr('y', null)
       .attr('transform', function (d) {
         return 'translate(' + ((d.source.y + d.target.y) / 2 - 30) + ',' + ((d.source.x + d.target.x) / 2 - 9) + ')';
       });
     linkText.exit().transition()
       .remove();

   }
  /**
   * actions to be taken if a functional edge box is closed
   * @param evt The action that was selected on the edge box
   */
  async edgeBoxClosed(evt) {

    if (evt[0] === 'node') {
      if (evt[1] === 'delete') {
        this.pruneNode(this.nodes, evt[2]);  // delete function node
      }
    }
    if (evt[0] === 'edge') {
      if (evt[1] === 'delete') {
        this.deletedEdges.push(evt[2]);
        if (evt[2].target.data.id !== 'Q1') {
          evt[2].target.data.type = 'new';
        }
        this.path = this.path.filter((x) => x !== evt[2]);
        this.coupling = this.coupling.filter((x) => x !== evt[2]);

      }
    }
    this.response = false;
    this.updateChart(this.nodes[0]);
    //this.plotCouplingEdges();
    //this.updateLinkText();
    //this.updateCouplingText();
    this.response = true;
  }

  /**
   * possible implementation of putting delay into a node
   * @param entry the entry node of the conversation
   */
  setTimeout(entry) {
    this.entry = entry;
    if (entry.length) {
      this.entry = JSON.parse(this.entry[0].CONFIG);
    }
    this.data.setScript([this.script, this.branch, this.entry.ENTRY_POINT]);  // save script and branch info
  }

  /**
   * Function that is used to draw normal, integration and functional nodes
   * @param evt The type of node selected on the side bar
   */
  nodeType(evt) {
    if (evt === 'q') {
      this.adNod = true;
      this.dragEndPos = null;
      this.typeNode = this.typeNode === 'Q' ? this.typeNode = '' : this.typeNode = 'Q';
    } else if (evt === 'f') {
      this.adNod = true;
      this.typeNode = this.typeNode === 'F' ? this.typeNode = '' : this.typeNode = 'F';
      this.dragEndPos = null;
    } else if (evt === 'i') {
      this.adNod = true;
      this.typeNode = this.typeNode === 'I' ? this.typeNode = '' : this.typeNode = 'I';
      this.dragEndPos = null;
    } else {
      this.adNod = false;
    }

  }

  /**
   * Function used to open the edge box when user clicks on an edge
   * label or when creating a new edge
   * @param type The type of the operation (new edge or existing edge)
   * @param d Reference to the current edge objecct
   */
  openEdgeDialog(type, d): void {
    const self = this;
    this.edgeDialogueMode = true;
    this.addEdgeMode = false;
    let edgeRef;
    d3.selectAll('.link').attr('display', 'block');   // display edge
    d3.selectAll(`#couplinglink_${new Date(d.tms).getTime()}`).style('stroke', 'rgb(255, 5, 5)');
    d3.selectAll(`#link_${new Date(d.tms).getTime()}`).style('stroke', 'rgb(255, 5, 5)');    // change its color to red
    d3.selectAll(`#linkLabel_${new Date(d.tms).getTime()}`).style('color', 'rgb(255, 5, 5)');  // change label color to red
    d3.selectAll(`#linkLabel_${new Date(d.tms).getTime()}`).style('font-weight', 'bold');
    if (d.source.data.id[0] === 'F') {
      const functionType = d.hasOwnProperty('label') && d.label !== '' ? 'output' : 'newedge';
      this.edgeCreated.emit([d, this.functions, this.nodes, functionType]);
    } else {
      if (type === 'new_edge' || type === 'existing_edge') {

        edgeRef = this.dialog.open(EdgeBoxComponent, {      // open the edge dialog
          width: '250px',
          panelClass: 'custom-edge-container',
          data: { links: this.path, edge: d, typeofedge: type, edgeid: d.id, coupling: this.coupling },  // ?
        });
      } else {
         edgeRef = this.dialog.open(EdgeBoxComponent, {
           width: '250px',
           panelClass: 'custom-edge-container',
           disableClose: true,
           data: { links: this.coupling, edge: d, typeofedge: type, edgeid: d.id, coupling: this.path },
         });
       }

      edgeRef.afterClosed().subscribe(result => {

        switch (result) {
          case 'cancel':
            if (this.historyObject.path.operation === 'create' || this.historyObject.path.operation === 'connection') {
              const nindex = this.nodes.findIndex((el) => el.data.id === this.historyObject.node.id);

              const pathfiltered = this.path.find((el) => !el.hasOwnProperty('edgetype'));
              if (pathfiltered) {
                this.deletedEdges.push(pathfiltered);
              }

              this.path = this.path.filter((x) => x !== pathfiltered);
              this.coupling = this.coupling.filter((x) => x !== pathfiltered);



              this.historyObject.node = {
                id: '',
                data: '',
                operation: ''
              };
              this.historyObject.path = {
                id: '',
                data: '',
                operation: '',
                pathtype: ''
              };

            }
            this.updateChart(this.nodes[0]);
            //this.plotCouplingEdges();
           // this.updateLinkText();
           // this.updateCouplingText();

            break;
          case 'delete':
            this.deletedEdges.push(d);   // put edge into deleted edges
            this.path = this.path.filter((x) => x !== d);
            this.coupling = this.coupling.filter((x) => x !== d);
            this.updateChart(this.nodes[0]);
            break;
          case 'save':
            this.edgeDialogueMode = false;
            if (d.edgetype !== 'button') {
              d.url = '';
              d.imgurl = '';
            }
            if (!d.hasOwnProperty('edgetype')) {
              d.edgetype = 'noinput';
            } else if (d.edgetype === '') {
              d.edgetype = 'noinput';
            }
            d3.selectAll('.link').style('stroke', '#ccc');
            d3.selectAll('.couplinglink').style('stroke', '#ccc');
            d.clicked = false;
            this.updateChart(this.nodes[0]);
           // this.plotCouplingEdges();
           // this.updateLinkText();
          //  this.updateCouplingText();
            break;
          default:
            this.edgeDialogueMode = false;
            if (d.edgetype !== 'button') {
              d.url = '';
              d.imgurl = '';
            }
            if (!d.hasOwnProperty('edgetype')) {
              d.edgetype = 'noinput';
            } else if (d.edgetype === '') {
              d.edgetype = 'noinput';
            }
            d3.selectAll('.link').style('stroke', '#ccc');
            d3.selectAll('.couplinglink').style('stroke', '#ccc');
            d.clicked = false;
            this.updateChart(this.nodes[0]);
           // this.plotCouplingEdges();
          //  this.updateLinkText();
          //  this.updateCouplingText();

        }
      });
    }
  }

  /**
   * Function that loads the node component when user creates a new node
   * or clicking on an existing node text box
   * @param id The id for the currently selected node
   */
  openNodeDialog(id): void {

    let source = this.root;    // get the root
    this.nodes[0].data.name = [];
    this.nodes[0].data.data.name = [];
    const tempnode = Object.assign({}, this.nodes[0]);
    let id_index = this.nodes.findIndex(x => x.data.id === id);
    d3.select('#node_butt_' + id).select('body').style('display', 'none');

    if (this.adNod && this.typeNode === 'F' && id[0] !== 'Q') {   // if a functional node is selected
      let idx;
      this.addednodes = this.nodes.reduce(function (max, obj) {
        return +obj.data.id.replace(/\D/g, '') > +max.data.id.replace(/\D/g, '') ? obj : max;
      });
      this.addednodes = +this.addednodes.data.id.replace(/\D/g, '') + 1;
      for (const i of this.nodes) {
        const ind = this.deletedNodes.findIndex((x) => x === `${this.typeNode}${this.addednodes}`);
        if (ind !== -1) {
          this.addednodes++;
        } else {
          break;
        }
      }

      const newNodeObj = {   // create object for the new node
        id: `${this.typeNode}${this.addednodes}`,
        name: [],
        type: 'new',
        node_param: null,
        node_label: null,
        attributes: [],
        children: []
      };

      const newNode = d3.hierarchy(newNodeObj);   // place new node on the graph
      newNode.node_param = '';
      newNode.node_label = '';
      newNode.depth = this.selected.depth + 1;
      newNode.height = this.selected.height - 1;
      newNode.parent = this.selected;
      newNode.id = `${this.typeNode}${this.addednodes}`;
      if (!this.selected.children) {
        this.selected.children = [];
        this.selected.data.children = [];
      }
      this.selected.children.push(newNode);
      this.selected.data.children.push(newNode.data);

      this.edgeCreated.emit([newNode, this.functions, this.nodes, 'other']);
    } else if (this.adNod === true && this.typeNode === 'Q' || id[0] === 'Q') {     // if selected node is normal

      const dialogRef = this.dialog.open(NodeBoxComponent, {     // open the node dialog box
        width: '400px',
        panelClass: 'custom-dialog-container',
        data: {
          nodedata: id_index !== -1 ? this.nodes[id_index].data : tempnode.data,
          nodes: this.nodes, links: this.path, coupling: this.coupling, functions: this.functions
        },
      });

      dialogRef.afterClosed().subscribe(result => {   // send infromation back to graph component
        let idx;
        if (result && result[0] === 'DEL') {   // if user clicked delete
          this.pruneNode(this.nodes, result[1].nodedata.data ? result[1].nodedata.data.id : result[1].nodedata.id);    // delete node
          // this.saveGraph();
        } else if (!result) {
          this.nMode = false;
        } else if (this.nMode === true) {      // new node is created
          if (id_index < 0) {
            this.addNode('new', '');
            idx = this.nodes.findIndex(x =>
              x.data.id === `${this.typeNode}${this.addednodes}`);
            this.selected = this.nodes[id_index];
            this.nodes[idx].data.name = tempnode.data.data.name;
            this.nodes[idx].data.node_label = tempnode.data.data.node_label;
            this.nodes[idx].data.node_param = tempnode.data.data.node_param;
            source = this.selected;
            id = `${this.typeNode}${this.addednodes}`;
            id_index = this.addednodes + 1;
            this.nMode = false;
          }
        }
        if (this.nodes[id_index] && this.nodes[id_index].data.data && this.nodes[id_index].data.data.name.length > 0) {
          this.nodes[id_index].data = this.nodes[id_index].data.data;
          this.newCompactNodeBox(id);   // create an empty node
          this.addNodeMode = true;
        } else if (this.nodes[id_index] && this.nodes[id_index].data && this.nodes[id_index].data.name.length) {
          this.newCompactNodeBox(id);
          this.addNodeMode = true;
        }
        this.updateChart(source);  // update the chart
      //  this.updateLinkText();     // update the link labels
      });

    }

  }

  /**
   * Create a compact node box when the user adds some text in th the node box
   * @param id The id of the currently selected node
   */
  async newCompactNodeBox(id) {
    d3.select('#node_butt_' + id).select('body').remove();
    await d3.select('#node_butt_' + id).append('xhtml:body')
      .html((d) => {
        if (d.data.name.length === 0) {
          return '<div class = "node_text new_node_text">Add...</div>';
        }
        let k = '<div class = "container">';
        if (typeof (d.data.node_label) === 'string') {
          k += '<div class = "edited"><span><xmp>' + d.data.node_label + '</xmp></span></div>';
        } else if (typeof (d.data.node_label) !== 'string') {
          if (typeof (d.data.name[0].node_text) === 'object') {
            k += '<div class = "edited"><span><xmp>' + JSON.stringify(d.data.name[0].node_text) + '</xmp></span></div>';
          } else {
            for (let i = 0; i < d.data.name.length; i++) {
              k += '<div class = "edited"><span><xmp>' + d.data.name[i].node_text + '</xmp></span></div>';
            }
          }
        } else if (typeof (d.data.name) !== 'string') {
          for (let i = 0; i < d.data.name.length; i++) {
            k += '<div class = "edited"><span><xmp>' + d.data.name[i].node_text + '</xmp></span></div>';
          }
        }
        return k + '</div>';
      });
  }


  /**
   * Function for adding a new node in the current graph
   * @param type The type of the add operation
   * @text the text of the newly created node
   */
  addNode(type, text) {
    const self = this;

    self.addednodes = this.nodes.reduce(function (max, obj) {
      return +obj.data.id.replace(/\D/g, '') > +max.data.id.replace(/\D/g, '') ? obj : max;
    });
    self.addednodes = +self.addednodes.data.id.replace(/\D/g, '') + 1;
    for (const i of this.nodes) {
      const ind = this.deletedNodes.findIndex((x) => x === `${this.typeNode}${this.addednodes}`);
      if (ind !== -1) {
        this.addednodes++;
      } else {
        break;
      }
    }
    if (type === 'first') {
      this.selected = this.dragStartPos;
    }

    // creates New OBJECT
    const newNodeObj = {
      id: `${this.typeNode}${this.addednodes}`,
      name: [],
      type: type,
      node_param: null,
      node_label: null,
      attributes: [],
      children: []
    };
    if (type === 'added') {
      newNodeObj.type = '';
      newNodeObj.name = text;
      newNodeObj.id = `${this.typeNode}${this.addednodes}`;
    }

    const newNode = d3.hierarchy(newNodeObj);
    newNode.node_param = '';
    newNode.node_label = '';
    newNode.depth = this.selected.depth + 1;
    newNode.height = this.selected.height - 1;
    newNode.parent = this.selected;
    newNode.id = `${this.typeNode}${this.addednodes}`;
  
    if (this.selected !== '') {
      if (this.selected._children) {
        this.dragStartPos.children = this.dragStartPos._children;
        this.dragStartPos._children = null;
      }
      if (!this.selected.children) {
        this.selected.children = [];
        this.selected.data.children = [];
      }
      if (type === 'added') {
        newNode.children = this.childs;
        newNode.data.children = this.child_data;
      }

      this.selected.children.push(newNode);
      this.selected.data.children.push(newNode.data);
      this.updateChart(this.selected);
     // this.plotCouplingEdges();
     // this.updateCouplingText();
      this.id++;
      return `${this.typeNode}${this.addednodes}`;
    }

  }

  /**
   * Function for searching a particular node text from a the node array
   */
  async navigateSearch(evt) {
    for (const i of this.searchPaths) {
      for (let j of i) {
        if (j !== 0) {
          j = j.match(/\d+/)[0];
        }
        if (this.nodes[j] && this.nodes[j]._children) {
          this.clusterNodes(this.nodes[j], '');
         
        }
      }
    }
  }

  /**
   * Function for highlighting the the selested text that was recently searched
   * @param text The text searhed by the user
   */
  async highlightText(text) {
    if (text === 'erase') {
      d3.select('#graph').remove();
      this.loadData();
    }
    
    const root = await this.graphService.flatten(this.root);

    const search = await this.searchpath.search(text, root);
    this.searchPaths = await this.searchpath.findSearchPaths(search);
    for (const i of this.searchPaths) {
      if (1) {
        for (let j of i) {
          if (j === 0) {
            j = `Q${j}`;
          }
          d3.select('#node_' + j).style('fill', (d) => {
            if (d.data.id === j) {
              if (d._children) {
                return '#dc8860';
              }
            }
          });
        }
      }

      d3.select('#node_' + i[1]).style('fill', () => {
        return '#dc8860';
      });
      d3.select('.fobject_' + i[1] + '_edited .edited').style('background', (d) => {
        return '#dc8860';
      });

    }
    if (search) {
      this.collapseNodes.emit(search.length);
    }
  }

   highlightLinks(node) {
    const outPath = this.path.filter((x) => x.source.data.id === node.data.id);
    const outCoupling = this.coupling.filter((x) => x.source.data.id === node.data.id);
    const inPath = this.path.filter((x) => x.target.data.id === node.data.id);
    const inCoupling = this.coupling.filter((x) => x.target.data.id === node.data.id);
    

    for (const outGoing of outPath) {
      d3.selectAll(`#link_${new Date(outGoing.tms).getTime()}`).style('stroke', '#0000ff');
      d3.selectAll(`#link_${new Date(outGoing.tms).getTime()}`).style('stroke-width', '2px');
    }

    for (const outGoing of outCoupling) {
      d3.selectAll(`#couplinglink_${new Date(outGoing.tms).getTime()}`).style('stroke', '#0000ff');
      d3.selectAll(`#couplinglink_${new Date(outGoing.tms).getTime()}`).style('stroke-width', '2px');
    }

    for (const inbound of inPath) {
      d3.selectAll(`#link_${new Date(inbound.tms).getTime()}`).style('stroke', '#ff0000');
      d3.selectAll(`#link_${new Date(inbound.tms).getTime()}`).style('stroke-width', '2px');
   }

    for (const inbound of inCoupling) {
      d3.selectAll(`#couplinglink_${new Date(inbound.tms).getTime()}`).style('stroke', '#ff0000');
      d3.selectAll(`#couplinglink_${new Date(inbound.tms).getTime()}`).style('stroke-width', '2px');
    }


   }

   highlightNodes(edge) {

    d3.selectAll('#node_' + edge.source.data.id).style('fill', '#dc8860');
    d3.selectAll('.fobject_' + edge.source.data.id + '_edited .edited').style('background','#dc8860');
    d3.selectAll('#node_' + edge.target.data.id).style('fill', '#dc8860');
    d3.selectAll('.fobject_' + edge.target.data.id + '_edited .edited').style('background','#dc8860');

   }
  /**
   * Function that is used to draw aline in the svg
   */
  drawLine(d) {
    const self = this;
    let xy0,
      path, k = 0;
    this.keep = false;
    const line = d3.line()
      .x(function (p) { return p[0]; })
      .y(function (p) { return p[1]; });

    d3.select('#graph')    // drawing of a newly created edge
      .on('mousedown', function (e) {
        if (self.dragStartPos) {
          this.keep = true;
          xy0 = d3.mouse(this);
          path = d3.select('#graph')
            .append('path')
            .attr('class', 'createnode')
            .attr('d', line([xy0, xy0]))
            .style('stroke', '#dc8860')
            .style('stroke-width', '2px');
        } else {
          return;
        }
      })
      .on('mouseup', function (event) {
        this.keep = false;
        if (k > 5) {
          setTimeout(function () {
            self.addNodeMode = true;
          }, 500);
        } else {
        }
        d3.selectAll('.createnode').remove();
        self.data.setEdgeValue([self.path, self.coupling]);
      })
      .on('mousemove', function () {
        k++;
        if (this.keep) {
          const Line = line([xy0, d3.mouse(this).map(function (x) { return x - 1; })]);

          d3.select('#graph')
            .select('.createnode').attr('d', Line);
        }
      });
  }

  /**
   * Function  That sets the source node when a user creates an edge
   * @param d The currently selected node
   */
  dragStart(d) {
    this.dragStartPos = d;
  }

  /**
   * Function that sets the target on an edge when the user mouse up over a node
   * @param d The currently selected node
   */
  dragEnd(d, createnode?) {

    let maxId;
    const self = this;
    this.dragEndPos = d;
    this.allPath.forEach(element => {
      maxId = Math.max.apply(Math, this.allPath.map(function (o) { return +o.id; }));
    });
    if (this.deletedEdges.length) {
      this.deletedEdges.find((x) => {
        if (x.target.data.id === this.dragEndPos.data.id) {
          if (this.dragEndPos.data.id !== 'Q1') {
            this.dragEndPos.data.type = 'new';
            if (this.dragEndPos.data.data !== undefined) {
              this.dragEndPos.data.data.type = 'new';
            }
          }
          this.deletedEdges = this.deletedEdges.filter((y) => y !== x);
        }
        return true;
      });
    }

      if (this.dragStartPos.data.type !== 'new' || (this.dragStartPos.data.data && this.dragStartPos.data.data.type !== 'new') ||
       !this.dragStartPos.data.hasOwnProperty('type')) {
       if (this.dragEndPos.data.type !== 'new' || (this.dragEndPos.data.data && this.dragEndPos.data.data.type !== 'new')) {
         if (this.dragStartPos !== this.dragEndPos) {
           if (this.dragEndPos.parent.data.id !== 'Q' || this.dragEndPos.data.id === 'Q1') {
             if (this.coupling.indexOf({ source: this.dragStartPos, target: this.dragEndPos }) === -1) {
                this.historyObject.path.pathtype = 'coupling';
                this.coupling.push({
                  source: this.dragStartPos, target: this.dragEndPos, edgetype: '', label: '', tms: new Date()
                });
             }
             this.addNodeMode = false;
             this.collapse = false;
             this.plotCouplingEdges();
              const index = this.coupling.findIndex(x =>
                x.source.id === this.dragStartPos.id && x.target.id === this.nodes[this.nodes.length - 1].id);
              this.openEdgeDialog('coupling_edge', this.coupling[this.coupling.length - 1]);
              setTimeout(function () {
                self.addNodeMode = true;
              }, 2000);
           }
         }
       }
     }

    if ((this.dragEndPos.data.type === 'new')
      || (this.dragEndPos.parent.data.id === 'Q' && this.dragEndPos.data.id !== 'Q1')) {
      this.selected = this.dragStartPos;
      if (this.dragStartPos && this.dragEndPos !== undefined) {
        if (this.dragStartPos.data.id !== this.dragEndPos.data.id) {
          if (this.dragStartPos.data.id !== `${this.typeNode}${this.addednodes}`) {
            this.addNodeMode = false;
            let text = this.nodes.find((x) => {
              return x.data.id === this.dragEndPos.data.id;
            });
            text = text !== undefined ? text.data.name : '';
            this.childs = this.dragEndPos.children;
            this.child_data = this.dragEndPos.data.children;
            this.dragEndPos.parent = this.dragStartPos;
            this.dragEndPos.data.type = 'added';
            if (this.dragEndPos.data.data) {
              this.dragEndPos.data.data.type = 'added';
            }
            const nodeArray = [];
            let parent = null;
            for (const node of this.nodes) {
              if (node.parent !== null) {
                parent = node.parent.data.id;
              }
              nodeArray.push({
                id: node.data.id, parent: parent,
                name: node.data.data ? node.data.data.name : node.data.name, node_param: node.data.node_param,
                node_label: node.data.node_label, type: node.data.type, orphan: node.data.orphan
              });
            }

            // Function that converts the the raw JSON from backend to d3 readable form

            this.treeData = d3.stratify()
              .id(function (d) { return d.id; })
              .parentId(function (d) {
                if (d.parent) {
                  return d.parent;
                }
              })
              (nodeArray);

            this.treeData.each(function (d) {
              d.name = d.data.name;
              d.node_param = d.data.node_param;
              d.node_label = d.data.node_label;
              d.type = d.data.type;
              d.orphan = d.data.orphan;
            });

            this.root = d3.hierarchy(this.treeData, (d) => d.children);
            this.root.x0 = this.height / 2;
            this.root.y0 =  10;
            // Collapse after the second level

              this.updateChart(this.root);
          //    this.plotCouplingEdges();
            //  this.updateCouplingText();
            //  this.updateLinkText();
              

            this.dragEndPos.data.type = 'added';
            if (this.dragEndPos.data.data) {
              this.dragEndPos.data.data.type = 'added';
            }
            maxId += 1;
            this.path.push({
              id: maxId, source: this.dragStartPos, target: this.dragEndPos,
              tms: new Date()
            });
            this.historyObject.path.pathtype = 'normal';

            this.addedEdges++;
            this.openEdgeDialog('new_edge', this.path[this.path.length - 1]);
          }
        }
      }
      d3.selectAll('.createnode').remove();
      setTimeout(function () {
        self.addNodeMode = true;
      }, 2000);
    }
    this.data.setEdgeValue([this.path, this.coupling]);
  }

  /**
   * Function used to delete a node from the graph
   * @param id The id of the node to be deleted
   */
  async pruneNode(array, id) {
    if (id !== this.entry.ENTRY_POINT) {
      if (id !== 'Q1' && id !== 'Q') {

        const nodeIndex = this.nodes.findIndex((x) => {
                    return x.data.id === id;
                  });

          this.deletedNodes.push(this.nodes[nodeIndex].data.id);
         // await this.updateChart(this.nodes[0], 'drag');
      }
    }
  }

  /**
   * Function that is used to collapse nodes
   * @param data_ The data to be collapsed
   */
  async clusterNodes(data_, type) {
    if (data_.children) {
      data_._children = data_.children;
      data_.children = null;
      if (type !== 'add_node') {
        this.collapse = true;
      }
    } else {
      data_.children = data_._children;
      data_._children = null;
      this.collapse = false;
    }
    
    await this.updateChart(data_);
   // await this.updateLinkText();
  //  await this.plotCouplingEdges();
  //  await this.updateCouplingText();
    await d3.selectAll('g').style('pointer-events', 'all');
   
  }

  /**
   * Function that saves the newly created graph using cxd to DB
   */
  async saveGraph() {
    let minId, maxId;
    this.response = false;
    const self = this;
    d3.select('#graph').remove();   // remove the whole graph
    this.updatedGraphData = false;

    // callpsed nodes patch -> collapsed nodes are saved and restored before graph is saved
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i]._children) {
        this.clusterNodes(this.nodes[i], '');
      }
    }

    

    const nodes = await this.graphService.convertToFlatNodeData(this.nodes);
    const path = this.path.filter((x) => x.source.data.id !== 'Q');
    this.path.forEach(element => {
      maxId = Math.max.apply(Math, this.path.filter((o) => o.id > 0).map((e) => +e.id));
    });
    if (maxId > 9) {
      minId = maxId + 1;
    } else {
       this.coupling.forEach(element => {
         minId = Math.min.apply(Math, this.coupling.filter((o) => o.id > 9).map((e) => +e.id));
       });
      minId = minId > 9 && minId !== Infinity ? minId : 10;
    }
    

    const edges = await this.graphService.convertToFlatEdgeData([path, this.coupling, minId]);

    this.data.getVarValue().subscribe(res => {
      if (res) {
        self.variables = res;
    }
     });
   
    

    console.log('saved nodes');
    console.log('-------------------')
    console.log(nodes);
    console.log('saved edges');
    console.log('-------------------')
    console.log(edges);
    await this.core.updateData(this.script, this.branch, nodes, edges, this.entry.ENTRY_POINT, this.variables).subscribe(
      response => {
        this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
          this.router.navigate([this.currentUrl]));
          this.variables = null;
        this.response = true;
        this.updatedGraphData = true;
      },
      err => console.log(err)
    );
  }

}
