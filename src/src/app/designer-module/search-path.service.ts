import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchPathService {
  searchPaths;

  constructor() { }

  async search(text, root) {
    // return new Promise((resolve, reject) => {
    const regexp = new RegExp(text.toLowerCase(), 'g');
    const search = [];
    for (const n of await root) {
      for (const name of n.data.name) {
        // console.log('hii', name)
        // if (name.node_text === text) {
        //   search.push(n);
        //   console.log(n);
        // }

        let match;
        const matches = [];

        while (typeof name.node_text === 'string' && (match = regexp.exec(name.node_text.toLowerCase())) != null) {
          matches.push(match.index);
        }
        // console.log(indices);
        // return indices;
        if (matches.length > 0) {
          search.push({ node: n, occurence: matches.length });
        }
      }
      if ((regexp.exec(n.data.node_label)) != null) {
        search.push({ node: n, occurence: 0 });
      } if ((regexp.exec(n.data.node_param)) != null) {
        search.push({ node: n, occurence: 0 });
      }
    }
    return search;
    // });
  }
  findSearchPaths(search) {
    this.searchPaths = [];
    for (const n of search) {
      let d = n.node;
      const pat = [0];
      if (d !== undefined) {
        while (d.parent) {
          // d3.selectAll("#node" + d.id).style("fill", "red");//color the node
          if (d.parent !== 'null') {
            //  d3.selectAll("#link" + d.parent.id + "-" + d.id).style("stroke", "red");//color the path
            pat.push(d.data.id);
            d = d.parent;
          }
        }
        this.searchPaths.push(pat);
      }
    }
    return this.searchPaths;
  }
}
