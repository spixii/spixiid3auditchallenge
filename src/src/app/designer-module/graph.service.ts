
interface Node {
  id: Number;
  name: [{
    node_text: '',
    parameter: '',
    label: ''
  }];
  parent: Number;
}

interface Edge {
  source: Number;
  edgetype: '';
  label: '';
  order: '';
  target: Number;
  url: '';
  imgurl: '';
  dashed: boolean;
}

interface Vars {
  id: Number;
  script: Number;
  branch: Number;
  nodeId: '';
  name: '';
  value: '';
  key: '';
}


export class GraphService {
  public convertToFlatNodeData(nodeData) {
    const nodes = [];
    let position = {
      x: '',
      y: '',
      parent: ''
    }
    for (const node of nodeData) {
      // console.log(node)
      if (node.parent !== null) {
        position['x'] = node.x0
        position['y'] = node.y0
        position['parent'] = node.parent.data.id;
        if (node.parent.data.id === `Q`) {
          node.parent.data.id = null;
        }
        nodes.push({
          id: node.data.id, parent: node.parent !== null ? node.parent.data.id : null,
          node_param: node.data.data !== undefined ?
          node.data.data.node_param : node.data.node_param, node_label: node.data.data !== undefined ?
          node.data.data.node_label : node.data.node_label, name: node.data.data !== undefined ? node.data.data.name : node.data.name,
          position: JSON.stringify(position)
        });
      } else {
        position['x'] = node.x0
        position['y'] = node.y0
        position['parent'] = 'Q'
      }
    }
    return nodes;
  }

  public convertToFlatEdgeData(edgeData) {
    const edges = [];
    let i = 5;
    for (const solidedge of edgeData[0]) {
      edges.push({
        edge_id: solidedge.source.data.id[0] !== 'F' ? i : solidedge.id,
        source: solidedge.source.hasOwnProperty('data') ? solidedge.source.data.id : solidedge.source
        , target: solidedge.target.hasOwnProperty('data') ? solidedge.target.data.id.toString() : solidedge.target,
        edgetype: solidedge.edgetype, label: solidedge.label, order: solidedge.order,key:solidedge.key, url: solidedge.url,
        imgurl: solidedge.imgurl, tms: solidedge.tms
      });
      i++;
    }
    let j = edgeData[2]
    for (const dasheddedge of edgeData[1]) {
      edges.push({
        edge_id: dasheddedge.source.data.id[0] !== 'F' ? j :  dasheddedge.id,
        source: dasheddedge.source.data.id, target: dasheddedge.target.data.id,
        edgetype: dasheddedge.edgetype, label: dasheddedge.label, order: dasheddedge.order, key:dasheddedge.key,url: dasheddedge.url,
        imgurl: dasheddedge.imgurl, tms: dasheddedge.tms
      });
      j+=1;
    }

    // edges = edges.filter((thing, index, self) =>
    //   index === self.findIndex((t) => (
    //     t.source === thing.source && t.target === thing.target
    //   ))
    // );

    return edges;
  }
  ///////////
  public flatten(root) {
    
    const nodes = [];
    let i = 0;

    function recurse(node) {
      if (node.children) { node.children.forEach(recurse); }
      if (node._children) { node._children.forEach(recurse); }
      if (!node.id) { node.id = ++i; }
      nodes.push(node);
    }

    recurse(root);
    return nodes;
  }

  public diagonal(d) {

    return 'M' + d.source.y + ',' + d.source.x
      + 'C' + (d.source.y + d.target.y) / 2 + ',' + d.source.x
      + ' ' + (d.source.y + d.target.y) / 2 + ',' + d.target.x
      + ' ' + d.target.y + ',' + d.target.x;
  }

  dashedDiagonal(d) {
    let p, c;
    if (d.source.y === d.target.y) {
      p = 2; c = 0;
    } else {
      p = 2; c = 80 * (d.pathCount);
    }
    return 'M' + d.source.y + ',' + d.source.x
      + 'C' + ((d.source.y + d.target.y) / p) + ',' + `${d.source.x + c}`
      + ' ' + ((d.source.y + d.target.y) / p) + ',' + `${d.target.x + c}`
      + ' ' + d.target.y + ',' + d.target.x;
  }

  getdistances(clickcoordinates, nodes) {
    const clickpoints = this.gettranslatedcoordinates(nodes);
    const distances = clickpoints.map((el) => {
      return Math.sqrt(Math.pow((el['x'] - clickcoordinates[0]), 2) + Math.pow((el['y'] - clickcoordinates[1]), 2));
    });
    return distances;
  }

  gettranslatedcoordinates(nodes) {
    let transx= 0, transy = 0;
    let cor = d3.select('#g-main').attr('transform');
    let gtranslate = cor.match(/[+-]?(\d*\.)?\d+/gi);
    const coordinatearray = nodes.map((el) => {
      const domx = (+el.y) + (+gtranslate[0]);
      const domy = (+el.x) + (+gtranslate[1]);
      return {
        id: el.data.id,
        x: domx,
        y: domy
      };
    });
    return coordinatearray;
  }

  attachToMainParent(nodeSet, entry) {  // import the entry
    for (const index in nodeSet) {   // set the first node's parent to 'Q'
      if (nodeSet.hasOwnProperty(index)) {
      if (nodeSet[index].id === entry.ENTRY_POINT) {
        nodeSet[index].parent = 'Q';
      } else if (nodeSet[index].parent === null) {
        nodeSet[index].parent = 'Q';
      } else if (nodeSet[index].id === 'Q1') {
        nodeSet[index].parent = 'Q';
      }

      // set properties for orphan node
      if (nodeSet[index]['orphan'] && nodeSet[index].id !== 'Q1' && nodeSet[index].id !== 'Q') {
        // nodeSet.splice(index, 1)
        try {
          const position = JSON.parse(nodeSet[index]['position']);
          nodeSet[index].parent = position['parent'];
         // nodeSet[index].data = {type:'new'};
         if (nodeSet[index].data) {
           nodeSet[index].data.type = 'new';
         }
        } catch (e) {
          console.log(e);
        }
      }
    }
  }
    nodeSet.push({    // put an empty node in the end of the node data
      'id': 'Q',
      'name': [],
      'parent': null,
      'node_param': null,
      'node_label': null,
    });

    if (nodeSet.length === 1 && nodeSet[0].id === 'Q') {  // todo if the graph contains only one node
      const firstNode = {
        'id': 'Q1',
        'name': [],
        'parent': 'Q',
        'node_param': null,
        'node_label': null,
        'type': 'added'
      };
      nodeSet.push(firstNode);
    }

    return nodeSet;
  }

}


