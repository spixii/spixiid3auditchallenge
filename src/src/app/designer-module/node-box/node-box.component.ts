import { Component, OnInit, Inject, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NodeTextMenuComponent } from '../node-text-menu/node-text-menu.component';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';

export interface DialogData {
  coupling;
  nodedata;
  nodes; 
  links;  
}

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-dialogue',
  templateUrl: './node-box.component.html',
  styleUrls: ['./node-box.component.css']
})
export class NodeBoxComponent implements OnInit {
  newNodeName = null;
  showLabelBox;
  paramLabel = '';
  nodeOption = '';
  index;
  menuOpen;
  bin;
  newMsg;
  tmpName = [];
  buttons;     // buttons ordering
  curr_text;
  plusbtn = false;
  clickedNewMsg = true;
  nodeData;    //data of this specific node
  param = null;
  label = null;
  OrderBox = false;
  menuItem = '';
  tmpFirstNodeText = 0;
  showNewNodebox = false;
  tempNodeData = { node_text: '', node_param: '', node_label: '' };

  @ViewChild(NodeTextMenuComponent) NodeTextMenuComponent: NodeTextMenuComponent;

  constructor(
    public dialogRef: MatDialogRef<NodeBoxComponent>, 
    private dialogue: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, 
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
    ) {
    iconRegistry.addSvgIcon(
      'three-dots',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/three-dots.svg'));
    iconRegistry.addSvgIcon(
      'tick',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/tick.svg'));
    iconRegistry.addSvgIcon(
      'node-bin',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/node-bin.svg'));
    iconRegistry.addSvgIcon(
      'close',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/close-round.svg'));
  }

  ngOnInit() {
    this.dialogRef.beforeClose().subscribe(() => {
      this.saveOnType(this.newNodeName, 'withoutplusbutton');
    });
    this.nodeData = {data: {}};
    if (this.data.nodedata.data === undefined) {
      this.nodeData.data = {
        id: this.data.nodedata.id, name: this.data.nodedata.name,
        type: this.data.nodedata.type, node_param: this.data.nodedata.node_param,
        node_label: this.data.nodedata.node_label
      };
    }

    // if (this.data.nodedata.data && this.data.nodedata.data.name[0]) {
    //   this.newNodeName = this.data.nodedata.data.name[0].node_text;
    //   this.data.nodedata.data.name.shift();
    // }
    // if (this.data.nodedata.data) {
    //   if (typeof (this.data.nodedata.data.name) === 'string') {
    //     const name = this.data.nodedata.data.name;
    //     this.nodeData.data.name = [{ node_text: name }];
    //   }
    // }
    // this.dialogRef.afterOpen().subscribe((res) => {
    if (this.data.nodedata.data && this.data.nodedata.data.name.length) {
      this.clickedNewMsg = false;
      this.plusbtn = true;
      this.tmpName = JSON.parse(JSON.stringify(this.data.nodedata.data.name));
    }
    else if (this.data.nodedata.name.length) {
      if (this.data.nodedata.data) {
        this.data.nodedata.data.name = this.data.nodedata.name;
      }
      this.clickedNewMsg = false;
      this.plusbtn = true;
      this.tmpName = JSON.parse(JSON.stringify(this.data.nodedata.name));
    }
    // });

    if (this.data.nodedata.name && this.data.nodedata.name.length === 0) {
      this.showNewNodebox = true;
    }
  }
  delMsg(i) {
    this.tmpName.splice(i, 1);
    if (i === 4) {
      this.clickedNewMsg = false;
    }
  }
  // hideBin() {
  //   this.bin = false;
  // }
  menuClick(item, index) {
    this.menuItem = '';
    this.index = index;
    this.menuItem = item;
    if (item === 'Delete Node') {
      // this.data.nodedata.data.name = [];
      // this.nodeData.data.name = [];
      // if (this.data.nodedata.data.name.length === 0) {
      //   this.showNewNodebox = true;
      // }
      this.dialogRef.close(['DEL', this.data]);
    } else if (this.menuItem === 'Assign Variable') {
      this.showLabelBox = true;
      this.paramLabel = 'Parameter';
      // this.nodeOption = this.data.nodedata.data.node_param;
      this.menuOpen = 'Variable';
    } else if (this.menuItem === 'Assign Label') {
      this.showLabelBox = true;
      this.paramLabel = 'Label';
      // this.nodeOption = this.data.nodedata.data.node_label;
      this.menuOpen = 'label';
    } else if (this.menuItem === 'Order of buttons') {

      this.buttons = this.data.links.filter(link => {
        return link.source.data.id === this.data.nodedata.id && link.edgetype === 'button';
      });

      this.buttons = this.buttons.concat(this.data.coupling.filter(link => {
        return link.source.data.id === this.data.nodedata.id && link.edgetype === 'button';
      })
      );
      console.log(this.buttons);


      if (this.buttons !== undefined) {
        this.buttons = this.buttons.sort((a, b) => (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0));
      }
      if (this.buttons.length > 0) {

        this.menuOpen = 'order';
      }
    }
  }
  // setLabeAndParam(i) {
  //   this.showLabelBox = false;
  //   if (this.paramLabel === 'Parameter') {
  //     // this.data.nodedata[i].node_param = this.nodeOption;
  //     this.data.nodedata.data.node_param = this.nodeOption;
  //     this.data.nodedata.data.node_label = null;
  //   } else if (this.paramLabel === 'Label') {
  //     this.data.nodedata.data.node_label = this.nodeOption;
  //     this.data.nodedata.data.node_param = null;
  //   }
  //   this.dialogue.closeAll();
  // }

  addLabel(evt) {
    this.data.nodedata.data ? this.data.nodedata.data.node_label = evt.target.value
      : this.data.nodedata.node_label = evt.target.value;
  }

  addVariable(evt) {
    this.data.nodedata.data ? this.data.nodedata.data.node_param = evt.target.value
      : this.data.nodedata.node_param = evt.target.value;
  }

  saveOnType(name, type) {
    this.clickedNewMsg = true;
    this.plusbtn = false;
    //  this.tmpName = [];
    if (type !== 'withoutplusbutton') {
      this.showNewNodebox = true;
    }
    if (name) {
      this.showLabelBox = false;
      this.tmpName.push({ node_text: this.newNodeName });
      //  this.tmpName.filter((x) => x.node_text !== '');
      // this.dialogRef.close();
      // this.tempNodeData.node_text = this.newNodeName;
      // if (this.tmpFirstNodeText === 0) {
      //   this.nodeData.data.name.unshift({ node_text: this.newNodeName });
      //   this.tmpFirstNodeText = 1;
      // } else if (this.nodeData.data.name.length < 4) {
      //  this.nodeData.data.name.push({ node_text: this.newNodeName });
      // }
      // this.data.nodedata.push({ node_text: name, label: this.nodeOption });
      this.nodeOption = '';
      this.newNodeName = null;
      // this.tempNodeData = { node_text: '', node_param: '', node_label: '' };
    }
  }

  menuOpened() {
    this.menuOpen = null;
  }

  saveNodeTexts() {
    if (!this.data.nodedata.node_label) {
      this.data.nodedata.node_label = null;
    }
    this.tmpName = this.tmpName.filter((x) => x.node_text !== '');
    this.nodeData.data.name = this.tmpName;
    if (this.data.nodedata.data) {
      this.data.nodedata.data.name = this.nodeData.data.name;
    }
    this.data.nodedata.name = this.nodeData.data.name;
    this.dialogRef.close(['SAV', this.data]);
  }

  deleteKeys() {
    this.data.links = this.data.links.filter(link => {
      if (link.source.data.id === this.data.nodedata.id) {
        link.key = null
      }
      return true;
    });

    this.data.coupling = this.data.coupling.filter(link => {
      if (link.source.data.id === this.data.nodedata.id){
        link.key = null;
      }
      return true;
    }
    );
  }

  cancel() {
  }
}

