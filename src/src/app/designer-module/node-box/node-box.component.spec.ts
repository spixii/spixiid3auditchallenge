import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeBoxComponent  } from './node-box.component';

describe('NodeBoxComponentt', () => {
  let component: NodeBoxComponent;
  let fixture: ComponentFixture<NodeBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
