import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftsideToolboxComponent } from './leftside-toolbox.component';

describe('LeftsideToolboxComponent', () => {
  let component: LeftsideToolboxComponent;
  let fixture: ComponentFixture<LeftsideToolboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftsideToolboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftsideToolboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
