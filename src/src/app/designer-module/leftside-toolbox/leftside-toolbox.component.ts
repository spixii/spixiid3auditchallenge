import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { GraphComponent } from '../graph/graph.component';
import { SearchPathService } from '../search-path.service';
// import * as cj from 'circular-json';
// import * as _ from 'lodash';
declare let Mark, $;
@Component({
  selector: 'app-leftside-toolbox',
  templateUrl: './leftside-toolbox.component.html',
  styleUrls: ['./leftside-toolbox.component.css']
})
export class LeftsideToolboxComponent implements OnInit {
  showSearchBox = false;
  searchText;
  searchColor = '#ffffff';
  markInstance;
  searchOccurences = 0;
  results;
  type = 'q';
  currentClass = 'current';
  currentIndex = 1;
  searchTick = true;
  searchNav = false;
  toggle = false;
  status = 'Enable';
  // private _data;
  @Output() highlightText = new EventEmitter();
  @Output() navigateSearch = new EventEmitter();
  @Output() nodeType = new EventEmitter();
  @Output() showVisitors = new EventEmitter();
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, search: SearchPathService) {
    iconRegistry.addSvgIcon(
      'node-standard',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/node-standard.svg'));

    iconRegistry.addSvgIcon(
      'search',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/search.svg'));
    iconRegistry.addSvgIcon(
      'navigate-arrow-right',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/navigate-arrow-right.svg'));
    iconRegistry.addSvgIcon(
      'tick',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/tick.svg'));
    iconRegistry.addSvgIcon(
      'star',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/star.svg'));
    iconRegistry.addSvgIcon(
      'triangle',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/triangle.svg'));
  }

  searchBox() {
    this.showSearchBox = this.showSearchBox === true ? false : true;
    if(this.showSearchBox === false) {
      this.highlightText.emit('erase');
      this.searchText = null;
    }
    this.searchColor = this.searchColor === '#ffffff' ? 'rgb(93, 135, 190)' : '#ffffff';
  }

  ngOnInit() {
    this.markInstance = new Mark(document.querySelector('#graph'));
    /**
     * Jumps to the element matching the currentIndex
    */
  }

  nodesType(type) {
    // this.toggle = !this.toggle;
    // if (this.type) {
    // this.type = null;
    if (this.type === type) {
      this.type = null;
    } else {
      this.type = type;
    }
    // } else {
    //   this.type = type;
    // }
    // }

    // if (this.type) {
    //   this.nodeType.emit(null);
    // } else {
      this.nodeType.emit(type);
    // }
  }

  nodeCreated(evt) {
    // if (evt === true) {
    //   this.nodeType.emit(null);
    //   this.toggle = !this.toggle;
    // }
  }

  collapseNodes(evt) {
    if (evt === 'col') {
      this.showSearchBox = false;
    } else if (evt === null) {
        this.highlight();
      } else {
        this.searchOccurences = evt;
      }
      // // const data = JSON.stringify(evt);
      // const search = cj.stringify(evt).search(`"node_text":"${this.searchText}"`);
      // if (search > -1) {
      // }
  }

  next() {
    //  this.$nextBtn.add(this.$prevBtn).on("click", function () {
    if (this.results.length) {
      this.currentIndex = this.currentIndex + 1;
      if (this.currentIndex < 0) {
        this.currentIndex = this.results.length;
      }
      if (this.currentIndex > this.results.length) {
        this.currentIndex = 1;
      }
      this.jumpTo();
    }
    // });
    this.navigateSearch.emit(null);
    this.highlightText.emit(this.searchText);
  }

  prev() {
    if (this.results.length) {
      this.currentIndex = this.currentIndex - 1;
      if (this.currentIndex < 0) {
        this.currentIndex = this.results.length;
      }
      if (this.currentIndex > this.results.length) {
        this.currentIndex = 1;
      }
      this.jumpTo();
    }

  }

  jumpTo() {
    if (this.results.length) {
      const current = this.results.eq(this.currentIndex - 1);
      this.results.removeClass(this.currentClass);
      if (current.length) {
        current.addClass(this.currentClass);
        //  position = $current.offset().top - this.offsetTop;
        //  window.scrollTo(0, position);
      }
    }
  }

  showTick() {
    this.searchTick = true;
    this.searchNav = false;
    if (this.searchText.length < 1) {
      this.markInstance.unmark();
    }
  }



  highlight() {
    const self = this;
    // const searchTerm = this.searchText;
    // this.markInstance.unmark().mark(searchTerm);
    self.highlightText.emit(this.searchText);
    if (this.searchText &&  this.searchText.length > 1) {
      this.searchNav = true;
      this.searchTick = false;
      const searchVal = this.searchText;
      self.markInstance.unmark({
        done: function () {
          self.markInstance.mark(searchVal, {
            separateWordSearch: true,
            done: function () {
              self.results = $('#graph').find('mark');
              if (self.results.length > 0) {
                self.currentIndex = 1;
              } else {
                self.currentIndex = 0;
              }
              self.jumpTo();
            }
          });
        }
      });
      this.searchOccurences = this.results.length + 1;
    } else {
      this.markInstance.unmark();
    }
  }
}
