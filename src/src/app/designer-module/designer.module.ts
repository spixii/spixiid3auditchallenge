import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// tslint:disable-next-line:max-line-length
import { MatToolbarModule, MatCheckboxModule, MatDialogModule, MatButtonToggleModule, MatSelectModule, MatAutocompleteModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatCardModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { NvD3Module } from 'ng2-nvd3';
import { DragulaModule } from 'ng2-dragula';
import { DesignerRoutingModule } from './designer-routing.module';
import { GraphComponent } from './graph/graph.component';
import { LeftsideToolboxComponent } from './leftside-toolbox/leftside-toolbox.component';
import { DesignerMainComponent } from './designer-main/designer-main.component';
import { NodeBoxComponent } from './node-box/node-box.component';
import { MatFormFieldModule } from '@angular/material';
import { MatMenuModule } from '@angular/material/menu';
import { MatInputModule } from '@angular/material';
import { NodeTextMenuComponent } from './node-text-menu/node-text-menu.component';
import { EdgeBoxComponent } from './edge-box/edge-box.component';
import { GraphService } from './graph.service';
import { FunctionalEdgeComponent } from './functional-edge/functional-edge.component';


@NgModule({
  imports: [
    NvD3Module,
    MatDialogModule,
    MatFormFieldModule,
    MatMenuModule,
    MatInputModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule,
    CommonModule,
    DesignerRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatIconModule,
    MatListModule,
    MatButtonToggleModule,
    MatAutocompleteModule,
    MatCardModule,
    DragulaModule.forRoot(),
    MatProgressSpinnerModule,
  ],
  declarations: [GraphComponent,
    LeftsideToolboxComponent,
    DesignerMainComponent,
    NodeBoxComponent,
    NodeTextMenuComponent,
    EdgeBoxComponent,
    FunctionalEdgeComponent],
  entryComponents: [NodeBoxComponent, EdgeBoxComponent],
  providers: [GraphService]
})
export class DesignerModule { }
