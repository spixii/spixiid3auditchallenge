import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-functional-edge',
  templateUrl: './functional-edge.component.html',
  styleUrls: ['./functional-edge.component.css']
})
export class FunctionalEdgeComponent implements OnInit {
  events: string[] = [];
  opened: boolean;
  functions;
  id;
  node;
  type;
  edge;
  output;
  nodes;
  argVals = [];
  farg;
  selected;
  functionControl = new FormControl('', [Validators.required]);
  // selectFormControl = new FormControl('', Validators.required);
  @ViewChild('sidenav') sidenav;
  @Output() edgeBoxClosed = new EventEmitter();

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  edgeCreated(evt) {
    console.log('functional node', evt)
    this.selected = undefined;
    this.functions = evt[1];
    if (evt[3] === 'output') {
      this.opened = true;
      this.node = undefined;
      this.edge = evt[0];
      this.selected = this.edge.label;
    } else if(evt[3] === 'newedge') {
      this.nodes = evt[2];
      this.edge = evt[0];
      this.node = undefined;
      const node = this.nodes.find((el)=>el.data.id === evt[0].source.data.id)
      if(node !== undefined && node['data'] !== undefined) {
        const fun = this.functions[0];
        this.farg = fun.farg;
        this.output = fun.fret;
        node['data']['name'] = [{ node_text: { val: JSON.stringify(this.farg), fval: this.functions[0].fname, fret: fun.fret } }];
      }
      if (node !== undefined && node['data']['data'] !== undefined) {
        const fun = this.functions[0];
        this.farg = fun.farg;
        this.output = fun.fret;
        if (!node['data']['data']['name'].length) {
          node['data']['data']['name'] = [{ node_text: { val: JSON.stringify(this.farg), fval: this.functions[0].fname, fret: fun.fret } }];
        }
        
      }
      this.edge.source = node
  } else {
      this.edge = undefined;
      this.node = evt[0];
      if (this.node.data.data !== undefined && this.node.data.data.name.length) {
        this.functionControl.setValue(this.node.data.data.name[0].node_text.fval)
      } else {
        this.functionControl.setValue(this.functions[0].fname);
      }     
      this.findFarg(0);
    }
    this.nodes = evt[2];
    this.type = evt[3];
    this.sidenav.toggle();
  }

  findFarg(type) {

    let fun;
    let fname;
    if (this.functions && type === 'new') {
      fun = this.functions.find((x) => x.fname === this.functionControl.value);
      if (fun !== null || fun !== undefined) {
        this.farg = fun.farg;
        this.output = fun.fret;
        return this.farg;
      }
    } else if (type === 0) {
      if (!this.node.data.name.length || (this.node.data.data !== undefined && !this.node.data.data.name[0].length)) {
        fname = this.functionControl.value;
        fun = this.functions.find((x) => x.fname === fname);
        if (fun) {
          this.farg = fun.farg;
          return this.farg;
        }
      } else {
        if (this.node.data.data !== undefined) {
          this.functionControl.setValue(this.node.data.data.name[0].node_text.fval);
          this.farg = JSON.parse(this.node.data.data.name[0].node_text.val);
        } else if(this.node.data.name.length) {
          this.functionControl.setValue(this.node.data.name[0].node_text.fval);
          this.farg = JSON.parse(this.node.data.name[0].node_text.val);
        }
      }
    }
  }

  sideNavClose() {
    this.opened = false;

    if (this.node !== undefined && this.node['data']['data'] !== undefined) {
      const fun = this.functions.find((x) => x.fname === this.functionControl.value);
      this.node['data']['data']['name'] = [{ node_text: { val: JSON.stringify(this.farg), fval: this.functionControl.value, fret: fun.fret } }];
      this.edgeBoxClosed.emit(['node', this.node, '']);
    } else if(this.node !== undefined && this.node['data'] !== undefined) {
      const fun = this.functions.find((x) => x.fname === this.functionControl.value);
      this.node['data']['name'] = [{ node_text: { val: JSON.stringify(this.farg), fval: this.functionControl.value, fret: fun.fret } }];
      this.edgeBoxClosed.emit(['node', this.node, '']);
    } else {
      const sel = this.edge.source.data.data.name[0].node_text.fret.find(x => x.text == this.selected);
      if (sel && sel.text !== "") {
        this.edge.label = this.selected;
        this.edge.id = sel.label_id;
        this.edge.edgetype = 'button';
        this.edgeBoxClosed.emit(['edge', '', '']);
      } else {
        this.delete();
        this.sidenav.close();
      }
    }
  }

  delete() {
    if (this.node) {
      this.edgeBoxClosed.emit(['node', 'delete', this.node.data.id]);
    } else {
      this.edgeBoxClosed.emit(['edge', 'delete', this.edge]);
    }
  }
}
