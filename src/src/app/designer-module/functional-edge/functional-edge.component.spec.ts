import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionalEdgeComponent } from './functional-edge.component';

describe('FunctionalEdgeComponent', () => {
  let component: FunctionalEdgeComponent;
  let fixture: ComponentFixture<FunctionalEdgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunctionalEdgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionalEdgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
