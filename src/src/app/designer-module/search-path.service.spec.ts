import { TestBed } from '@angular/core/testing';

import { SearchPathService } from './search-path.service';

describe('SearchPathService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchPathService = TestBed.get(SearchPathService);
    expect(service).toBeTruthy();
  });
});
