import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DesignerMainComponent } from './designer-main/designer-main.component';

const routes: Routes = [

  { path: 'script/:script_id/branch/:branch_id/build', component: DesignerMainComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DesignerRoutingModule { }
