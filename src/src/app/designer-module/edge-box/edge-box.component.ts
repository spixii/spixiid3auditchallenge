import { Component, OnInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

export interface EdgeData {
  links;
  typeofedge;
  edgeid;
  edge;
  coupling;
}

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-edge-box',
  templateUrl: './edge-box.component.html',
  styleUrls: ['./edge-box.component.css'],
})
export class EdgeBoxComponent implements OnInit {
  viewButtonBox = false;
  label = '';
  buttonControl = new FormControl();
  allButtons = [];
  currentEdge;
  filteredOptions: Observable<string[]>;
  url;
  events: string[] = [];
  opened: boolean;
  urlGroup = false;
  edge;
  edge_id;
  b_label;
  active;
  inp_color = false;
  linkImg = false;
  link = false;
  button_id = 0;
  constructor(
    public edgeRef: MatDialogRef<EdgeBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EdgeData, 
    iconRegistry: MatIconRegistry, 
    sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'arrow-calendar',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/arrow-calendar.svg'));
    iconRegistry.addSvgIcon(
      'arrow-freetext',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/arrow-freetext.svg'));
    iconRegistry.addSvgIcon(
      'three-dots',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/three-dots.svg'));
    iconRegistry.addSvgIcon(
      'arrow-noinput',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/arrow-noinput.svg'));
    iconRegistry.addSvgIcon(
      'arrow-button',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/arrow-button.svg'));
    iconRegistry.addSvgIcon(
      'arrow-image',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/arrow-image.svg'));
    iconRegistry.addSvgIcon(
      'arrow-link',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/arrow-link.svg'));
    iconRegistry.addSvgIcon(
      'arrow-link-image',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/arrow-link-image.svg'));
    iconRegistry.addSvgIcon(
      'down-arrow',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/down-arrow.svg'));
  }

  ngOnInit() {
    this.buttonControl.disable();
    this.edge = this.data.edge;

    if (this.edge.edgetype !== 'button') {
      this.edge.label = '';
      this.url = this.edge.url;
      if (this.edge.edgetype === 'freetext') {
        this.active = -1;
        this.freeText();
      } else if (this.edge.edgetype === 'calendar') {
        this.active = -2;
        this.addCalendar();
      } else {
        this.active = 0;
        this.noInput();
      }
    } else {
      if (this.edge.label) {
        this.active = 1;
        this.buttonControl.enable();
        this.buttonControl.setValue(this.edge.label);
        this.b_label = this.edge.label;
      }
    }
    this.currentEdge = this.data.links[this.edge];
    this.data.links.filter((value, index, array) => {
      if (value.edgetype === 'button' && array.indexOf(value) === index) {
        this.allButtons.push({ id: value.id, value: value.label });
      }
    });
    const cButtons = [];
    // this.data.coupling.filter((value, index, array) => {
    //   if (value.edgetype === 'button' && array.indexOf(value) === index) {
    //     cButtons.push({ id: value.id, value: value.label });
    //   }
    // });
    this.allButtons = this.allButtons.concat(cButtons);
    this.allButtons = Array.from(new Set(this.allButtons));
    this.filteredOptions = this.buttonControl.valueChanges.pipe(
      startWith(''),
      map(value => value && value.length >= 1 ? this._filter(value) : [])
    );
    this.buttonControl.valueChanges.subscribe(x => {
      this.edge.label = x;
    });
  }

  addButton() {
    this.edge_id = 1;
    this.buttonControl.enable();
    this.inp_color = true;
    //   this.button_id = this.data.links.length;
    // let max = 0;
    //   for (const link of this.data.links) {
    //     if (link.edgeid > max) { max = link.edgeid; }
    //   }
    // if (this.data.links[this.edge].edgetype === 'button') {
    //   this.label = this.data.links[this.edge].label;
    //   this.url = this.data.links[this.edge].url;
    //   this.buttonControl.setValue(this.label);
    // } else {
    //   this.data.links[this.edge].edgetype = 'button';
    //   this.data.links[this.edge].label = '';
    // }
    this.viewButtonBox = true;

    return '1';
  }

  saveButton() {
    if (this.buttonControl.value === '' && this.edge.edgetype === 'button') {
      return;
    }
    if (this.edge_id === 0) {
      this.edge.edgetype = 'noinput';
      this.edge.label = '';
      this.edge.edgeid = '0';
      this.edge.url = null;
      this.edge.imgurl = null;
    } else if (this.edge_id === -1) {
      this.edge.edgetype = 'freetext';
      this.edge.label = '';
      this.edge.edgeid = '-1';
      this.edge.url = null;
      this.edge.imgurl = null;
    } else if (this.edge_id === -2) {
      this.edge.edgetype = 'calendar';
      this.edge.label = '';
      this.edge.edgeid = '-2';
      this.edge.url = null;
      this.edge.imgurl = null;
    } else if (this.edge_id === 1) {
      if (this.buttonControl.value) {
        this.edge.edgetype = 'button';
        this.edge.order = 0;
        // this.edge.edgeid = '';
        this.buttonControl.setValue(this.edge.label);
      } else {
        return;
      }
    }
    this.edgeRef.close('save');

  }

  noInput() {
    this.edge_id = 0;
    this.buttonControl.disable();
    this.inp_color = false;
    this.viewButtonBox = false;

    return '0';

  }

  addCalendar() {
    this.edge_id = -2;
    this.inp_color = false;
    this.viewButtonBox = false;

    return '-2';

  }

  freeText() {
    this.edge_id = -1;
    this.inp_color = false;
    this.buttonControl.disable();
    this.viewButtonBox = false;
    return '-1';
  }
  
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    const distinctbuttons = [];
    const map = new Map();
    for (const item of this.allButtons) {
        if(!map.has(item.value)){
            map.set(item.value, true);    // set any value to Map
            distinctbuttons.push({
                id: item.id,
                value: item.value
            });
        }
    }
    return distinctbuttons.filter(option => {
      if (option.value) {
        return (option.value.toLowerCase().indexOf(filterValue) === 0);
      }
    });
  }

  addUrl() {
    if (this.link) {
      this.urlGroup = false;
    }
  }
  linkType(link) {
    this.urlGroup = !this.urlGroup;

    if (link === 'link') {
      this.link = true;
      this.linkImg = false;
    } else {
      this.linkImg = true;
      this.link = false;
    }
  }
  deleteEdge() {
    this.edgeRef.close('delete');
  }
  cancel() {
    if (this.buttonControl.value === '' && this.edge.edgetype === 'button') {
      return;
    } else {
      this.buttonControl.setValue(this.b_label);
      this.edgeRef.close('cancel');
    }
    if (this.edge.edgetype !== 'button') {
      this.edge.url = null;
    }
  }
}
