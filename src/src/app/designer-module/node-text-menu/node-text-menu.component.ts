import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { MatMenu, MatMenuTrigger } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { DragulaService } from 'ng2-dragula';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
@Component({
  selector: 'app-node-text-menu',
  templateUrl: './node-text-menu.component.html',
  styleUrls: ['./node-text-menu.component.css']
})
export class NodeTextMenuComponent implements OnInit {
  myControl = new FormControl();
  @ViewChild(MatMenu) menu: MatMenu;
  @Output() selectMenu = new EventEmitter();
  itemList;
  menuHide = true;
  menuAlive: boolean;
  dragulaModel;
  msg = '';
  value;
  nodePlaceholder;
  labArray = [];
  parArray = [];
  filteredOptions: Observable<string[]>;
  @Input() checkmenu;
  @Input() buttons: any[];
  @Input() currNode;
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  
  constructor(
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    private dragula: DragulaService
    ) {
    iconRegistry.addSvgIcon(
      'three-dots',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/three-dots.svg'));
    iconRegistry.addSvgIcon(
      'tick',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/tick.svg'));
    this.itemList = [
      { item: 'Delete Node' },
      {
        item: 'Assign Label'
      },
      {
        item: 'Assign Variable'
      }, {
        item: 'Message Settings'
      }, {
        item: 'Order of buttons'
      }];
  }

  ngOnInit() {
    this.myControl.setValue(null);
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => val.length >= 1 ? this._filter(val) : [])
      );
    this.dragula
      .drop()
      .subscribe(value => {

        if (this.buttons !== undefined) {
          let i = 0;
          for (const button of this.buttons) {
            button.order = i;
            i++;
          }
        }
        setTimeout(() => {
        }, 1000);
      });
  }

  selectedMenu(item) {
    this.myControl.setValue(null);
    if (item === 'Assign Label') {
      this.myControl.setValue(this.currNode.nodedata.data ? this.currNode.nodedata.data.node_label : this.currNode.nodedata.node_label);
    }
    if (item === 'Assign Variable') {
      this.myControl.setValue(this.currNode.nodedata.data ? this.currNode.nodedata.data.node_param : this.currNode.nodedata.node_param );
    }
    this.selectMenu.emit(item);
    if (item === 'Order of buttons') {
     // this.checkmenu = false;
     
    }
    if (item === 'Delete Message') {
      // this.checkmenu = false;
      if (this.currNode.nodedata.data.name.length) {
        this.currNode.nodedata.data.name = [];
        this.currNode.nodedata.data.node_label = null;
      } else if (this.currNode.nodedata.name.length) {
        this.currNode.nodedata.name = [];
        this.currNode.nodedata.data.node_param = null;
      }
    }
  }

  onType($event: any) {
    $event.stopPropagation();
  }

  addLabelOrVar() {

    if (this.checkmenu === 'label') {
      if (this.currNode.nodedata.data) {
        this.currNode.nodedata.data.node_label = this.myControl.value;
      } else if (this.currNode.nodedata) {
        this.currNode.nodedata.node_label = this.myControl.value;
      }
    } else {
      if (this.currNode.nodedata.data) {
        this.currNode.nodedata.data.node_param = this.myControl.value;
      } else if (this.currNode.nodedata) {
        this.currNode.nodedata.node_param = this.myControl.value;
      }
    }
    this.trigger.closeMenu();
  }

  checkPlaceHolder() {
    if (!this.checkmenu) {
      this.nodePlaceholder = null;
      return;
    } else if (this.checkmenu === 'label') {
      this.nodePlaceholder = 'Enter Label';
      return;
    } else if (this.checkmenu === 'Variable') {
      this.nodePlaceholder = 'Enter Variable';
    }
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    for (const node of this.currNode.nodes) {
      if (node.data.data) {
        if (node.data.data.node_label) {
          this.labArray.push(node.data.data.node_label);
        } else if (node.data.node_label) {
          this.labArray.push(node.data.node_label);
        }
      }
      if (node.data.data) {
        if (node.data.data.node_param) {
          this.parArray.push(node.data.data.node_param);
          // if (this.parArray) {
          //   this.parArray = parArray.filter(function (item, pos) {
          //     return parArray.indexOf(item) === pos;
          //   });
          // }
        } else if (node.data.node_param) {
          this.parArray.push(node.data.node_param);
          // if (this.parArray) {
          //   this.parArray = parArray.filter(function (item, pos) {
          //     return parArray.indexOf(item) === pos;
          //   });
          // }
        }
      }
    }
    if (this.checkmenu === 'label') {
      if (this.labArray) {
        this.labArray = Array.from(new Set(this.labArray));
        return this.labArray.filter(option => option.toLowerCase().includes(filterValue));
      }
    } else if (this.checkmenu === 'Variable') {
      if (this.parArray) {
        this.parArray = Array.from(new Set(this.parArray));
        return this.parArray.filter(option => option.toLowerCase().includes(filterValue));
      }
    }
  }
}

