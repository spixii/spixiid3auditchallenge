import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeTextMenuComponent } from './node-text-menu.component';

describe('NodeTextMenuComponent', () => {
  let component: NodeTextMenuComponent;
  let fixture: ComponentFixture<NodeTextMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeTextMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeTextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
