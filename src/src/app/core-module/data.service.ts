import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  nodeData;
  edgeData;
  nodes;
  edges;
  private graphvalueObs: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  private valueObs: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  private scriptObs: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  public setNodeValue(value: string): void {
    this.graphvalueObs.next(value);
    // this.convertToFlatNodeData(value);
  }

  public getNodeValue(): Observable<string> {
    this.nodeData = this.graphvalueObs.asObservable();
    return this.nodeData;
  }
  public setEdgeValue(value): void {
    this.graphvalueObs.next(value);
    // this.convertToFlatEdgeData(value);
  }

  public getEdgeValue(): Observable<string> {
    this.edgeData = this.graphvalueObs.asObservable();
    return this.edgeData;
  }

  public setScript(scriptInfo: any): void {
    console.log(scriptInfo)
    this.scriptObs.next(scriptInfo);
  }

  public getScript(): Observable<any> {
    return this.scriptObs.asObservable();
  }
  public setVarValue(value: any): void {
    this.valueObs.next(value);
    setTimeout(() => {
      this.valueObs.next(null)
    }, 1000);
  }

  public getVarValue(): Observable<any> {
    return this.valueObs.asObservable();
  }

 

}

