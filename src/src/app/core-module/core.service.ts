import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class CoreService {

  constructor(private httpClient: HttpClient) { }

  updateData(script, branch, nodes, edges, entry, variables) {
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post(`${environment.apiBase}/update-graph`,
      {
        script: script, branch: branch, nodes: { nodes }, edges: { edges },
        entry: entry, variables:variables
      }, httpOptions).pipe(map((response: any) => response));
  }
  // assesed
  loadData(script, branch) {
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post(`${environment.apiBase}/load-graph`, { script: script, branch: branch }, httpOptions).pipe(map((response: any) => response));
  }

  loadPathsTree(script, branch, dates?) {
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post(`${environment.apiBase}/load-tree`, { script: script, branch: branch, dates: dates }, httpOptions).pipe(map((response: any) => response));
  }
   
  updateVarMap(script, branch, edgeId, newKey, src, tgt) {
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post(`${environment.apiBase}/update-var-map`, { script: script, branch: branch, edgeId: edgeId, newKey: newKey, src: src, tgt: tgt }, httpOptions).pipe(map((response: any) => response));
  }

  loadVarMap(script, branch) {
    return this.httpClient.post(`${environment.apiBase}/load-var-map`,
      { script: script, branch: branch }, httpOptions).pipe(map((response: any) => response));
  }

  publishScript(script, branch, entry) {
    return this.httpClient.post(`${environment.apiBase}/publish`,
      { script: script, branch: branch, entry }, httpOptions).pipe(map((response: any) => response));
  }
  
    // assesed
  loadScriptlist() {
    return this.httpClient.post(`${environment.apiBase}/scriptlist`,
      { post: 'ok' }, httpOptions).pipe(map((response: any) => response));
  }

  modifyScript(scriptName, scriptId, cloneId) {
    console.log(scriptId);
    return this.httpClient.post(`${environment.apiBase}/modify-script`,
      { script_name: scriptName, tms: new Date(), script_id: scriptId, clone_script_id: cloneId }, httpOptions)
      .pipe(map((response: any) => response));
  }

  modifyModule(scriptId, module) {
  
    return this.httpClient.post(`${environment.apiBase}/modify-script`,
      { script_id: scriptId, module: module }, httpOptions)
      .pipe(map((response: any) => response));
  }

  deleteScript(scriptId) {
    console.log(scriptId);
    return this.httpClient.post(`${environment.apiBase}/delete-script`,
    { script: scriptId}, httpOptions).pipe(map((response: any) => response));
  }
   
    // assesed
  loadVersions(id) {
    return this.httpClient.post(`${environment.apiBase}/versions`,
      { script: id }, httpOptions).pipe(map((response: any) => response));
  }

  addbranch(scrpt, descr, branch, parent) {
    return this.httpClient.post(`${environment.apiBase}/add-branch`,
      { script: scrpt, desc: descr, branch: branch, parent: parent }, httpOptions).pipe(map((response: any) => response));
  }

  addDesc(scrpt, descr, branch) {
    return this.httpClient.post(`${environment.apiBase}/add-desc`,
      { script: scrpt, desc: descr, branch: branch }, httpOptions).pipe(map((response: any) => response));
  }
}
