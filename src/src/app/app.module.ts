import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from '@angular/cdk/layout';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared-module/shared.module';
import { DesignerModule } from './designer-module/designer.module';
import { AnalyticsModule } from './analytics-module/analytics.module';
import { ScriptModule } from './script-module/script.module';
import { DataService } from './core-module/data.service';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';



@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MalihuScrollbarModule.forRoot(),
    BrowserAnimationsModule,
    MatIconModule,
    MatListModule,
    SharedModule,
    DesignerModule,
    AnalyticsModule,
    ScriptModule,
    HttpClientModule
  ],
  providers: [DataService, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
