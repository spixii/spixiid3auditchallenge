import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CoreService } from '../../core-module/core.service';
import { FormControl, Validators } from '@angular/forms';

export interface DialogData {
  decr: string;
  branch: number;
  script: number;
  parent: number;
}


@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
export class DescriptionComponent implements OnInit {
  showSave = true;
  errorMsg = '';
  sc_name = new FormControl('', [Validators.required, Validators.maxLength(4000)]);
  constructor(public dialogRef: MatDialogRef<DescriptionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private core: CoreService) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    if (this.data.branch === 0) {
      this.showSave = false;
    }
    this.sc_name.setValue(this.data.decr);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  getErrorMessage() {
    return this.sc_name.hasError('required') ? 'You must enter a value' :
      this.sc_name.hasError('maxlength') ? 'Exceeds 256 chars' :
        '';
  }
  saveDescr() {
    if (!this.sc_name.invalid) {
      this.data.decr = this.sc_name.value;
      // this.core.addDesc(this.data.script, this.data.decr, this.data.branch).subscribe((res) => {
        this.dialogRef.close(this.data.decr);
      // });
    }
    this.errorMsg = this.getErrorMessage();
  }
}
