import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared-module/shared.module';
import { ScriptTableComponent } from './script-table/script-table.component';
import { ScriptRoutingModule } from '../script-module/script-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatIconModule, MatTableModule, MatCardModule, MatMenuModule,
  MatSortModule, MatButtonModule, MatInputModule, MatFormFieldModule
} from '@angular/material';
import { CreateScriptComponent } from './create-script/create-script.component';
import { ScriptnbranchMainComponent } from './scriptnbranch-main/scriptnbranch-main.component';
import { VersionComponent } from './version/version.component';
import { NvD3Module } from 'ng2-nvd3';
import { DescriptionComponent } from './description/description.component';
import { DeleteScriptComponent } from './delete-script/delete-script.component';



@NgModule({
  imports: [
    CommonModule,
    MatSortModule,
    MatMenuModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatCardModule,
    MatIconModule,
    ScriptRoutingModule,
    MatButtonModule,
    MatInputModule,
    NvD3Module,
    MatFormFieldModule
  ],
  declarations: [ScriptTableComponent, DeleteScriptComponent,
     CreateScriptComponent, ScriptnbranchMainComponent, VersionComponent, DescriptionComponent, DeleteScriptComponent],
  entryComponents: [CreateScriptComponent, DescriptionComponent, DeleteScriptComponent],
})
export class ScriptModule { }
