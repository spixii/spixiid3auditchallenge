import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CreateScriptComponent } from '../create-script/create-script.component';
import { CoreService } from '../../core-module/core.service';

@Component({
  selector: 'app-scriptnbranch-main',
  templateUrl: './scriptnbranch-main.component.html',
  styleUrls: ['./scriptnbranch-main.component.css']
})


export class ScriptnbranchMainComponent implements OnInit {

  script_created = false;
  selScrId = 0;
  selTms;
  delScr;
  testEnabled = false;
  enabled = '#00ff00';
  disabled = '#ff6961';
  module;
  selBotId;
  constructor(public dialog: MatDialog, private core: CoreService) { }

  ngOnInit() {
  }
  openDialog(): void {
    this.script_created = false;
    const dialogRef = this.dialog.open(CreateScriptComponent, {
      width: '300px',
      panelClass: 'custom-script-container',
      data: { script_id: 'new' },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.script_created = true;
    });
  }

  scriptSelected($evt) {
    this.selScrId = 0;
    this.selScrId = $evt[0];
    this.selTms = $evt[1];
    this.selBotId = $evt[3];
    if ($evt[2]) {
      this.module = $evt[2];
      this.testEnabled = $evt[2].allow_unpublished;
    } else {
      this.testEnabled = false;
    }
    console.log(this.testEnabled);
  }
  scriptDel($evt) {
    this.delScr = $evt;
  }

  editModule() {
    if (this.module) {
      this.module.allow_unpublished = this.testEnabled;
      this.core.modifyModule(this.selScrId, this.module).subscribe((res) => {
    });
  }
  }



}
