import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScriptnbranchMainComponent } from './scriptnbranch-main.component';

describe('ScriptnbranchMainComponent', () => {
  let component: ScriptnbranchMainComponent;
  let fixture: ComponentFixture<ScriptnbranchMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScriptnbranchMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScriptnbranchMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
