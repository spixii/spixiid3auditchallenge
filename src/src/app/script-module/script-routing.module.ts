import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScriptnbranchMainComponent} from './scriptnbranch-main/scriptnbranch-main.component';

const routes: Routes = [

  { path: 'scriptlist', component: ScriptnbranchMainComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScriptRoutingModule { }
