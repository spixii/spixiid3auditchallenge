import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CoreService } from '../../core-module/core.service';
import { FormControl, Validators } from '@angular/forms';

export interface DialogData {
  script_id: number;
  scriptName: string;
  clientName: string;
  clone_script_id: number;
}

@Component({
  selector: 'app-create-script',
  templateUrl: './create-script.component.html',
  styleUrls: ['./create-script.component.css']
})

export class CreateScriptComponent implements OnInit {

  sc_name = new FormControl('', [Validators.required, Validators.maxLength(256)]);
  errorMsg = '';

  constructor(
    public dialogRef: MatDialogRef<CreateScriptComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private core: CoreService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  saveScript() {
    if (!this.sc_name.invalid) {
      this.data.scriptName = this.sc_name.value;
      this.core.modifyScript(this.data.scriptName, this.data.script_id, this.data.clone_script_id).subscribe((res) => {
        if (res.status === 'OK') {
          this.dialogRef.close();
        }
      });
    }
    this.errorMsg = this.getErrorMessage();
  }
  getErrorMessage() {
    return this.sc_name.hasError('required') ? 'You must enter a value' :
      this.sc_name.hasError('maxlength') ? 'Exceeds 256 chars' :
        '';
  }
  ngOnInit() {
    this.sc_name.setValue(this.data.scriptName);
  }

 

}
