import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CoreService } from '../../core-module/core.service';
import { DescriptionComponent } from '../description/description.component';
import { Router } from '@angular/router';

declare const d3, $;

@Component({
  selector: 'app-version',
  templateUrl: './version.component.html',
  styleUrls: ['./version.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VersionComponent implements OnInit {
  margin: any;
  width: number;
  height: number;
  svg: any;
  description: any;
  duration: number;
  root: any;
  tree: any;
  treeData: any = [];
  nodes: any;
  links: any;
  selected;
  id = 0;
  keep;
  dbmode = true;
  typeNode = 'Q';
  spinner = '#6576BF';
  response = false;
  extractDashed = false;
  collapse;
  addednodes = 0;
  coords;
  live_branch;
  editLabelMode = true;
  addNodeMode = true;
  addEdgeMode = false;
  dragStartPos;
  total_node_text = 0;
  branch;
  allPath;
  currentNode;
  dragEndPos = null;
  path = [];
  addedEdges = 0;
  edgeDialogueMode = false;
  coupling = [];
  couplingEdges = 0;
  message;
  multipleEdge;
  searchPaths = [];
  script;
  scr_id;
  tms;
  showver = true;
  entry;
  updatedGraphData = true;
  prevTransform;
  botId;

  @Input()
  set setScr(p: any) {
    if (p) {
      this.scr_id = p;
      d3.select('.group').remove();
      this.core.loadVersions(p).subscribe((res) => {
        this.loadData(res[0]);
        this.live_branch = res[1];
        this.addednodes = res[0].length;
      });
    }
  }
  @Input()
  set setTms(p: any) {
    if (p) {
      this.tms = p;
    }
  }

  @Input()
  set setId(p: any) {
    if (p) {
      this.botId = p
    }
  }
  @Input()
  set setDele(p: any) {
    if (p) {
      this.showver = p;
    }
  }
  constructor(public dialog: MatDialog, private core: CoreService, private router: Router) {
    this.id = 0;
  }

  ngOnInit() {
    const self = this;

  }

  openDialog(name, id, parent): void {
    const dialogRef = this.dialog.open(DescriptionComponent, {
      width: '300px',
      panelClass: 'custom-script-container',
      data: { script: this.scr_id, decr: name, branch: +id, parent: +parent },
    });

    dialogRef.afterClosed().subscribe(result => {
      let prnt;
      if (result) {
        this.addNode('added');
        if (this.selected.data.parent === null) {
          prnt = 1;
        } else {
          prnt = this.selected.data.id.replace(/^\D+/g, '');
        }
        d3.select('.group').remove();
        this.core.addbranch(this.scr_id, result,
          this.selected.children[this.selected.children.length - 1].data.id.replace(/^\D+/g, ''), prnt).subscribe(() => {
            this.core.loadVersions(this.scr_id).subscribe((res) => {
              this.loadData(res[0]);
              this.live_branch = res[1];
            });
          });
      }
    });

  }

  async loadData(res) {
    const node_data = [];
    for (const branch of res) {
      node_data.push({
        'id': `Q${branch.BRANCH}`,
        'name': branch.DESCR,
        'parent': branch.BRANCH_PARENT ? `Q${branch.BRANCH_PARENT}` : null,
      });
    }
    this.setNodeData([node_data, []]);

  }

  async setNodeData(data) {
    const self = this;
    const nodeData = data[0];

    this.treeData = d3.stratify()
      .id(function (d) { return d.id; })
      .parentId(function (d) { return d.parent; })
      (nodeData);

    // assign the name to each node
    this.treeData.each(function (d) {
      d.name = d.data.name;
    });
    // Set the dimensions and margins of the diagram
    this.margin = { top: 0, right: 90, bottom: 30, left: 0 };
    this.width = 500 - this.margin.left - this.margin.right;
    this.height = 400 - this.margin.top - this.margin.bottom;
    this.svg = d3.select('#branch')
      .attr('width', '100vw')
      .attr('height', '100%')
      .on('click', (d) => {
        if (this.addNodeMode !== false && this.editLabelMode === true) {
          self.coords = d3.mouse(d3.event.currentTarget);
          d3.select('#branch').call(d3.zoom().on('zoom', zoomed));        
          d3.selectAll('foreignObject').remove();
          this.addNodeMode = false;
        }
      })
      .append('g')
      .attr('class', 'group')
      .attr('transform', (d) => {
        return 'translate(' + 50 + ',' + 50 + ')';
      });
    
    d3.select('body')
      .on('keydown', (d) => {
        if (d3.event.keyCode === 46) {
          this.addNodeMode = true;
        }
      });

    function zoomed() {
      if (d3.event.type == "zoom") {
        if (d3.event.transform.k >= 0.4 && d3.event.transform.k <= 1.7) {
          self.svg.attr('transform', d3.event.transform);
          self.prevTransform = d3.event.transform
        } else {
          self.prevTransform.x = d3.event.transform.x
          self.prevTransform.y = d3.event.transform.y
          self.svg.attr('transform', self.prevTransform);   
          }       
      } else {
        self.svg.attr('transform', d3.event.transform);
      }
       
    }

    this.duration = 0;

    // declares a tree layout and assigns the size
    // Assigns parent, children, height, depth
    this.root = d3.hierarchy(this.treeData, (d) => d.children);
    this.root.x0 = this.height / 2;
    this.root.y0 = 10;
    // Collapse after the second level
    await this.updateChart(this.root);
  }

  addNode(type) {
    // creates New OBJECT
    this.addednodes++;
    const newNodeObj = {
      id: `${this.typeNode}${this.addednodes}`,
      name: [],
      type: '',
      node_param: '',
      node_label: '',
      attributes: [],
      children: []
    };
    if (type === 'added') {
      newNodeObj.type = '';
      newNodeObj.name = [];
      newNodeObj.id = `${this.typeNode}${this.addednodes}`;
    }

    // Creates new Node
    for (const n of this.nodes) {
      if (`Q${this.addednodes}` === n.id) {
        this.addednodes = this.nodes.length + 1;
      }
    }
    const newNode = d3.hierarchy(newNodeObj);
    newNode.node_param = '';
    newNode.node_label = '';
    newNode.depth = this.selected.depth + 1;
    newNode.height = this.selected.height - 1;
    newNode.parent = this.selected;
    newNode.id = this.addednodes;
    if (this.selected !== '') {
      if (this.selected._children) {
        this.selected.children = this.selected._children;
        this.selected._children = null;
      }
      if (!this.selected.children) {
        this.selected.children = [];
        this.selected.data.children = [];
      }
      this.selected.children.push(newNode);
      this.selected.data.children.push(newNode.data);

      this.updateChart(this.selected);
      this.id++;

    }

  }

  async updateChart(source) {
    let id = 0;
    if (source !== undefined) {
      this.tree = d3.tree()
        .size([this.height, this.width])
        .nodeSize([30, 0])
        .separation(function separation(a, b) {
          return a.parent === b.parent ? 3 : 3;
        });
      const self = this;
      let linkid = 0;
      this.treeData = this.tree(this.root);
      this.nodes = this.treeData.descendants();
      this.links = this.tree(this.treeData).links();

      this.dbmode = false;
      await this.coupling.filter(a => {
        return a.source !== undefined;
      });
      this.addedEdges = this.path.length;
      this.nodes.forEach((d) => { d.y = d.depth * 100 + 50; d.x = d.x + 50; });
      const node = this.svg.selectAll('g.node')
        .data(this.nodes, (d) => ++id);

      const nodeEnter = node.enter().append('g')
        .attr('class', 'node')
        .attr('transform', (d) => {
          source.y0 = source.y0;
          source.x0 = source.x0;
          return 'translate(' + source.y0 + ',' + source.x0 + ')';
        })
        .on('click', (d) => {
          this.router.navigate(['/script', self.scr_id, 'branch', +d.data.id.replace(/^\D+/g, ''), 'build']);

        })
        .on('mouseover', (d) => {
          // self.openDialog(d.data.name, d.data.id.replace(/^\D+/g, ''), 0);
          self.selected = d;
          self.addNodeMode = false;
          setTimeout(function () {
            self.addNodeMode = true;
          }, 1000);
          d3.selectAll('foreignObject').remove();
          const div = d3.select('#branch').select('.group').append('foreignObject')
            .attr('width', 161)
            .attr('height', 105)
            .attr('x', -79)
            .attr('y', -92)
            .attr('class', 'branch-node')
            .attr('transform', () => {
              return 'translate(' + d.y0 + ',' + d.x0 + ')';
            })
            .append('xhtml:body')
            .attr('class', 'tooltip')
            .style('opacity', 0);
          div.transition()
            .duration(200)
            .style('opacity', 9)
            .style('position', 'absolute');
          div.html((c) => {
            return `<div class="branch-card" >
            <p class ="tms">${this.tms}</p>
            <p class ="scr">${d.data.name ? d.data.name : ''}</p>
            <p class = "url-link"><a href="https://ue.dev.app.spixii.ai/chatbot/web/?spixii_bot_id=${this.botId}&b=${d.data.id.replace(/^\D+/g, '')}">Test branch here</a></p>
            </div><div class="branch-${d.data.id}" id="add-branch">+</div>`;
          });
          this.selected = d;
          div.select('#add-branch').on('click', () => {
            self.openDialog('', this.addednodes + 1, d.data.id);
          });
          div.selectAll('#add-branch').style('display', 'none');
          div.select(`.branch-${d.data.id}`).style('display', 'block');
        });

      nodeEnter.append('path')
        .style('stroke', function (d) {
          if (d.data.id[0] === 'Q') { return 'lightsteelblue'; } else
            if (d.data.id[0] === 'F') { return 'red'; } else {
              return 'green';
            }
        })
        .attr('class', 'nodesymbol')
        .style('fill', function (d) { return '#fff'; })
        .attr('d', d3.symbol()
          .size(function () { return 700; })
          .type(function (d) {
            {
              if (d.data.id[0] === 'Q') { return d3.symbolSquare; }
            }
          }))
        .style('fill', (d) => {
          let col = '#fff';
          if (d.data.id === `Q${self.live_branch}`) {
            col = `#6576BF`;
          }

          return d._children ? 'lightsteelblue' : col;
        });

      nodeEnter.append('text')
        .attr('dx', (d) => {
          if (d.data.id.replace(/^\D+/g, '') > 9) {
            return -5;
          } else {
            return -3;
          }
        })
        .attr('dy', '0.35em')
        .data(this.nodes)
        .text(function (d) {
          return d.data.id.replace(/^\D+/g, '');
        })
        .style('stroke', (d) => {
          if (+d.data.id.replace(/^\D+/g, '') === this.live_branch) {
            return '#fff';
          }
        });
    
      node.select('circle.node')
        .attr('r', 4.5)
        .style('fill', function (d) {
          return d._children ? 'lightsteelblue' : '#fff';
        });
      const nodeUpdate = nodeEnter.merge(node);
      nodeUpdate.transition()
        .duration(this.duration)
        .attr('transform', (d) => {
          if (self.coords !== undefined) {
            self.coords[0] = self.coords[0] - 1;
            self.coords[1] = self.coords[1] - 1;
          }
          if (d.data.type === 'new') {
            return 'translate(' + self.coords[0] + ',' + self.coords[1] + ')';
          } else {
            return 'translate(' + (d.y) + ',' + (d.x) + ')';
          }
        });

      nodeUpdate.select('circle.node')
        .attr('r', 5)
        .style('stroke-width', '2px')
        .style('stroke', 'steelblue')
        .style('fill', (d) => {
          if (d.data.id === 0) {
            return 'red';
          }
          if (d._children) {
            return 'lightsteelblue';
          }
        })
        // .text((d) => {
        // return d.data.name;
        // }
        // )
        .attr('cursor', 'pointer');

      const nodeExit = node.exit().transition()
        .duration(this.duration)
        .style('opacity', 1e-6).remove();

      const link = this.svg.selectAll('path.link')
        .data(this.links, (d) => d.id || (d.id = ++linkid));
        
      const linkArrow = this.svg.append('defs').selectAll('marker')
        .data(this.links)
        .enter()
        .append('marker')
        .attr('id', 'arrowhead')
        .attr('viewBox', '0 -5 10 10')
        .attr('refX', 15)
        .attr('refY', -0.5)
        .attr('markerWidth', 6)
        .attr('markerHeight', 6)
        .attr('orient', 'auto')
        .style('stroke-width', '1px')
        .style('fill', '#5d87be')
        .append('path')
        .attr('d', 'M0,-5L10,0L0,5');

      const linkEnter = link.enter().insert('path', 'g')
        .attr('class', 'link')
        .style('fill', 'none')
        .style('stroke', '#ccc')
        .style('stroke-width', '2px')
        .attr('display', (d) => {
          if (d.target.data.orphan === true) {
            return 'none';
          }
        })
        .attr('d', function (d) {
          // console.log("path test"+d);
          // const o = { x: d.source.x0, y: d.source.y0 };
          return self.diagonal(d);
        })
        .attr('marker-end', 'url(#arrowhead)')
        .style('stroke-width', '1px')
        .attr('d', function (d) {
          // var o = {x: d.source.x0, y: d.source.y0};
          // return this.diagonal({source: o, target: o});
        })
        .on('click', (d) => {
          self.addNodeMode = false;
          d3.select('#branch').on('mousedown.zoom', null);
          setTimeout(function () {
            self.addNodeMode = true;
          }, 2000);
          const edge = this.path.find(x => x.source.data.id === d.source.data.id &&
            x.target.data.id === d.target.data.id);
        });

      const linkUpdate = linkEnter.merge(link);

      linkUpdate.transition()
        .duration(this.duration)
        .attr('d', (d) => self.diagonal(d));

      const linkExit = link.exit().transition()
        .duration(this.duration)
        .attr('d', function (d) {
          const o = { x: source.x, y: source.y };
          return self.diagonal(d);
        })
        .remove();
      // d3.select('svg').on('keydown', function (d) {
      // console.log((d));
      // });

      this.nodes.forEach((d) => {
        d.x0 = d.x;
        d.y0 = d.y;
      });
      // console.log(this.nodes, this.path, this.couplingEdges);

    }

  }

  flatten(root) {
    const nodes = [];
    let i = 0;

    function recurse(node) {
      if (node.children) { node.children.forEach(recurse); }
      if (node._children) { node._children.forEach(recurse); }
      if (!node.id) { node.id = ++i; }
      nodes.push(node);
    }

    recurse(root);
    return nodes;
  }
  diagonal(d) {

    return 'M' + d.source.y + ',' + d.source.x
      + 'C' + (d.source.y + d.target.y) / 2 + ',' + d.source.x
      + ' ' + (d.source.y + d.target.y) / 2 + ',' + d.target.x
      + ' ' + d.target.y + ',' + d.target.x;
  }
  
  dashedDiagonal(d) {
    let p, c;
    // console.log(d.source.height, d.target.height);
    if ((d.target.height === (d.source.height + 1)) || ((d.source.height - 1) ===
      d.target.height) || ((d.source.height) === d.target.height - 1) || d.source.x === d.target.x) {
      p = 2.5; c = 30;
    } else {
      p = 2; c = 0;
    }
    return 'M' + d.source.y + ',' + d.source.x
      + 'C' + ((d.source.y + d.target.y) / p) + ',' + `${d.source.x + c}`
      + ' ' + ((d.source.y + d.target.y) / p) + ',' + `${d.target.x + c}`
      + ' ' + d.target.y + ',' + d.target.x;
  }
  
  prune(array, id) {
    if (id !== this.entry.ENTRY_POINT) {
      for (let i = 0; i < array.length; ++i) {
        const obj = array[i];
        if (id === 'new') {
          if (obj.data.type === 'new') {
            array.splice(i, 1);
            return true;
          }
        } else {
          if (obj.data.id === id) {
            const nodeIndex = this.nodes.findIndex((x) => {
              return x.data.id === id;
            });
            const delchilds = this.flatten(this.nodes[nodeIndex]);
            for (const child of delchilds) {
              this.path = this.path.filter((x) => {
                return x.source.data.id !== child.data.id;
              });
              this.path = this.path.filter((x) => {
                return x.target.data.id !== child.data.id;
              });
              this.coupling = this.coupling.filter((x) => {
                return x.source.data.id !== child.data.id;
              });
              this.coupling = this.coupling.filter((x) => {
                return x.target.data.id !== child.data.id;
              });

            }
            array.splice(i, 1);
            return true;
          }
        }
        if (obj.children) {
          if (this.prune(obj.children, id)) {
            if (obj.children.length === 0) {
              delete obj.children;
            }
            return true;
          }
        }
      }
      // this.couplingEdge('jj');
    }
  }


  saveGraph() {
    this.response = false;
    d3.select('#branch').style('display', 'none');
    // console.log('nod', this.nodes);
    this.updatedGraphData = false;
}

}
