import { Component, OnInit, ViewChild, EventEmitter, AfterViewInit, OnDestroy, Input, Output } from '@angular/core';
import { MatSort, MatTableDataSource, MatToolbarRow } from '@angular/material';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { CoreService } from '../../core-module/core.service';
import { MatDialog } from '@angular/material';
import { CreateScriptComponent } from '../create-script/create-script.component';
import { DeleteScriptComponent } from '../delete-script/delete-script.component';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';


export interface ScriptTable {
  id: any;
  Script: string;
  Client: string;
  Created_on: Date;
  
}
export interface Data {
  id: any;
  name: String;
}

/**
 * @title Table with sorting
 */
@Component({
  selector: 'app-script-table',
  templateUrl: './script-table.component.html',
  styleUrls: ['./script-table.component.css']
})
export class ScriptTableComponent implements OnInit, AfterViewInit, OnDestroy {
  displayedColumns: string[] = ['id', 'branch', 'botId', 'Script', 'Client', 'Created_on', 'Actions'];
  dataSource;
  ELEMENT_DATA;
  message: boolean;
  sel_script_id;
  sel_script_name;
  del_scr = true;
  
  script_created = false;
  @Output() scriptSelected = new EventEmitter();
  @Output() scriptDel = new EventEmitter();
  @Input()
  set setProp(p: boolean) {
    if (p) {
      this.loadScripts();
    }
  }

  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private mScrollbarService: MalihuScrollbarService,
    private core: CoreService,
    public dialog: MatDialog,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon(
      'node-bin',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/node-bin.svg'));
    iconRegistry.addSvgIcon(
      'three-dots',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/three-dots.svg'));
    iconRegistry.addSvgIcon(
      'clone-button',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/clone-button.svg'));
  }

  ngOnInit() {
    this.loadScripts();
  }
  ngAfterViewInit() {
    this.mScrollbarService.initScrollbar('#dark-card', { axis: 'y', theme: 'dark-thick' });
  }
  loadScripts() {
    {
      this.ELEMENT_DATA = [];
      this.core.loadScriptlist().subscribe((res) => {
        let tms = null;
        if (res) {
          for (const script of res) {
            const scr_name = JSON.parse(script.CONFIG);
            if (script.TMS !== null) {
              tms = script.TMS.substring(10, 0);
            }
  
            this.ELEMENT_DATA.push(
              {  id: script.SCRIPT,
                 branch: script.BRANCH,
                 botId: script.BOT_ID,
                 Script: scr_name.NAME,
                 Config: scr_name.module,
                 Client: '',
                 Created_on: new Date(tms).toJSON().slice(0, 10) },
            );
          }
          this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
          this.dataSource.sort = this.sort;
          this.dataSource.sortingDataAccessor = (data, header) => {
              if (header == "Script") {
                  return data[header].toLowerCase()
              } else {
                  return data[header];
              }
          }
        //  this.dataSource.sort = this.sort;
        }
      });
    }
  }
  editScript(id): void {
    const edit_script = this.ELEMENT_DATA.find((x) => x.id === id);
    const dialogRef = this.dialog.open(CreateScriptComponent, {
      width: '300px',
      panelClass: 'custom-script-container',
      data: { scriptName: edit_script.Script, clientName: null, script_id: edit_script.id },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadScripts();
      this.sel_script_id = undefined;
    });
  }

  deleteScript(id, name) {
    this.del_scr = false;
    this.scriptDel.emit(this.del_scr);
    const dialogRef = this.dialog.open(DeleteScriptComponent, {
      width: '300px',
      panelClass: 'custom-script-container',
      data: { id: id, name: name },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadScripts();
      this.sel_script_name = undefined;
    });
  }

  cloneScript(id) {
    this.script_created = false;
    const clone_script = this.ELEMENT_DATA.find((x) => x.id === id);
    const dialogRef = this.dialog.open(CreateScriptComponent, {
      width: '300px',
      panelClass: 'custom-script-container',
      data: { script_id: 'clone', clone_script_id: clone_script.id },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.script_created = true;
      this.loadScripts();
    });
  }

  ngOnDestroy() {
    this.mScrollbarService.destroy('#dark-card');
  }

  scrSelected(id, tms, module, botId) {
   // this.del_scr = false;
    this.scriptSelected.emit([id, tms, module, botId, '']);
  }
  selScript(id, name) {
    this.sel_script_id = id;
    this.sel_script_name = name;
  }
}
