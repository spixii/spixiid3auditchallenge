import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteScriptComponent } from './delete-script.component';

describe('DeleteScriptComponent', () => {
  let component: DeleteScriptComponent;
  let fixture: ComponentFixture<DeleteScriptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteScriptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteScriptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
