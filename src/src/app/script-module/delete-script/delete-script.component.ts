import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CoreService } from '../../core-module/core.service';


export interface DialogData {
  id: number;
  name;
}
@Component({
  selector: 'app-delete-script',
  templateUrl: './delete-script.component.html',
  styleUrls: ['./delete-script.component.css']
})


export class DeleteScriptComponent implements OnInit {


  constructor(public dialogRef: MatDialogRef<DeleteScriptComponent>, private core: CoreService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
  }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  deleteScript() {
    this.core.deleteScript(this.data.id).subscribe((res) => {
      this.dialogRef.close();
    });

  }

}
