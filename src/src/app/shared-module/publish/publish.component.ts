import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DataService } from '../../core-module/data.service';
import { CoreService } from '../../core-module/core.service';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';


export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-publish',
  templateUrl: './publish.component.html',
  styleUrls: ['./publish.component.css']
})
export class PublishComponent implements OnInit {
  @Output() highlightText = new EventEmitter();
  publish = false;
  script;
  branch;
  entry;
  botId;

  constructor(public dialogRef: MatDialogRef<PublishComponent>, private dataService: DataService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private coreService: CoreService,
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon(
      'check-circle',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/check-circle.svg'));
}

ngOnInit() {
  this.dataService.getScript().subscribe(val => {
    // console.log(val);
    this.script = val[0];
    this.branch = val[1];
    this.entry = val[2];

    this.coreService.loadScriptlist().subscribe((res) => {
        for (const scr of res) {
          if (this.script == scr.SCRIPT) {
            this.botId = scr.BOT_ID;
            break;
          }
        }
    });



  });
}
onNoClick(): void {
  this.dialogRef.close();
}

onClickAction() {
  this.publish = !this.publish;
  this.dialogRef.close();
}

saveScript() {

    this.coreService.publishScript(this.script, this.branch, this.entry).subscribe(
      response => {
        this.publish = true;
      },
      err => console.log(err)
    );
}

}
