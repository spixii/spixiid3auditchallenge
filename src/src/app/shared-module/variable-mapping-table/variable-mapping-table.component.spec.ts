import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariableMappingTable } from './variable-mapping-table.component';

describe('VariableMappingTable', () => {
  let component: VariableMappingTable;
  let fixture: ComponentFixture<VariableMappingTable>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariableMappingTable ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariableMappingTable);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
