import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { DataService } from '../../core-module/data.service';
import { CoreService } from '../../core-module/core.service';
import { ActivatedRoute } from '@angular/router';
import { BreakpointObserver } from '@angular/cdk/layout';

declare const $;

@Component({
	selector: 'var-map-tab',
	templateUrl: './variable-mapping-table.component.html',
	styleUrls: ['./variable-mapping-table.component.css']
})
export class VariableMappingTable implements OnInit {

	dataSet;
	script;
	branch;
	events: string[] = [];
	opened: boolean;
	@ViewChild('varmaptab') varmaptab;
	
	constructor(private breakpointObserver: BreakpointObserver, private route: ActivatedRoute,
		public coreService: CoreService, private data: DataService, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
	}

	ngOnInit() {
		this.data.getScript().subscribe(res => {
			if (res) {
				this.script = res[0];

				this.branch = res[1];

				this.coreService.loadVarMap(this.script, this.branch).subscribe(
					response => {
						if (response[0].length > 0) {
							this.dataSet = response[0];

						} else {
							this.dataSet = []
							const var_data = [];
						}

					},
					err => {
						this.dataSet = []
						return console.log(err);
					});
			}

		});
	}


	updateVar(varid, value): void {
		const dataSet = this.dataSet;
		for (const i in dataSet) {
			if (dataSet[i].id == varid) {
				dataSet[i].key = value;

				this.coreService.updateVarMap(dataSet[i].script, dataSet[i].branch,
					dataSet[i].edgeId, dataSet[i].key, dataSet[i].source, dataSet[i].target).subscribe(
						response => { },
						err => {
						});
				break;

			}
		}
	}

	Maptoggle() {
		
		this.varmaptab.toggle();
	}

	mapClosed(){
		this.data.setVarValue(this.dataSet)
	}

	

}
