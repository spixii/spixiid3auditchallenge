import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatMenu } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';

@Component({
  selector: 'options-menu',
  templateUrl: './options-menu.component.html',
  styleUrls: ['./options-menu.component.css']
})
export class OptionsMenuComponent implements OnInit {

  events: string[] = [];
  opened: boolean;

  @Output() menuOpen = new EventEmitter();

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'gear-menu',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/settings-gear.svg'));

  }

  ngOnInit() {
    /**
     * Jumps to the element matching the currentIndex
    */
  }
  toggleOptionsMenu() {
    const self = this;

    self.menuOpen.emit('options');
  }
}
