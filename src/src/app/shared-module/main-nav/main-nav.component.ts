import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { DataService } from '../../core-module/data.service';
import { MatDialog } from '@angular/material';
import { PublishComponent } from '../publish/publish.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';





export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit {
  links = ['Build', 'Analyse'];
  activeLink;
  animal: string;
  name: string;
  response = '';
  nodeDataSet;
  edgeDataSet;
  script;
  branch;
  @ViewChild('vartab') varmaptab;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  constructor(private breakpointObserver: BreakpointObserver, public _location: Location,
    private route: ActivatedRoute, private router: Router, public dialog: MatDialog,
    private data: DataService, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    // this.coreService.getData().subscribe((response: HttpResponse<any>) => {
    //   this.response = response.body;
    //   console.log(response.body);
    // });
    this.activeLink = this.links[0];
    iconRegistry.addSvgIcon(
      'publish',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/publish.svg'));
    iconRegistry.addSvgIcon(
      'navigate-arrow',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/navigate-arrow-left.svg'));
  }


  ngOnInit() {
    this.data.getNodeValue().subscribe((val) => {
      this.nodeDataSet = val;
    });
    this.data.getEdgeValue().subscribe((val) => {
      this.edgeDataSet = val;
    });
   
   if (this._location.path().search('build') > 0) {
     this.activeLink = this.links[0];
   } else
        if (this._location.path().search('analyse') >0 ) {
          this.activeLink = this.links[1];
        }

    
  }
  openDialog(): void {
    if (this.nodeDataSet.length > 0) {
      const dialogRef = this.dialog.open(PublishComponent, {
        width: '300px',
        height: '158px',
        panelClass: 'publish-script',
        data: { name: this.name, animal: this.animal }
      });

      dialogRef.afterClosed().subscribe(result => {
        // console.log('The dialog was closed');
        this.animal = result;
      });
    }
  }
  menuop($event) {
    this.varmaptab.Maptoggle();
  }
  navBack() {

    this.router.navigate(['/scriptlist']);
  }

  async openTab(link) {

   /* this.data.getScript().subscribe((script) => {
      this.script = script[0];
    });
    this.data.getScript().subscribe((script) => {
      this.branch = script[1];
    });*/
    const numbers = this._location.path().match(/\d+/g).map(String);
    this.script = numbers[0];
    this.branch = numbers[1];

    if (link === 'Build') {
      await  this.router.navigate(['/script', this.script, 'branch', +this.branch.replace(/^\D+/g, ''), 'build']);
    } else {
      await  this.router.navigate(['/script', this.script, 'branch', +this.branch.replace(/^\D+/g, ''), 'analyse']);
      }
  }
}
