import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedRoutingModule } from './shared-routing.module';
import { MainNavComponent } from './main-nav/main-nav.component';
import { VariableMappingTable } from './variable-mapping-table/variable-mapping-table.component';
import { OptionsMenuComponent } from './options-menu/options-menu.component';
import { LayoutModule } from '@angular/cdk/layout';
import { DesignerModule } from '../designer-module/designer.module';
import { MatToolbarModule, MatRadioModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { PublishComponent } from './publish/publish.component';
import { MatMenuModule } from '@angular/material/menu';
import { SubnavComponent } from './subnav/subnav.component';
import {MatTabsModule} from '@angular/material/tabs';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatRadioModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    DesignerModule,
    MatTabsModule
  ],
  exports: [
    MainNavComponent,
    SubnavComponent
  ],
  declarations: [
    MainNavComponent,
    VariableMappingTable,
    PublishComponent,
    OptionsMenuComponent,
    SubnavComponent
  ],
  entryComponents: [PublishComponent],
})
export class SharedModule { }
