export const environment = {
    production: false,
    test: true,
    dev: false,
    apiBase: 'https://ue.dev.app.spixii.ai/cxd_mockup/app',
};
