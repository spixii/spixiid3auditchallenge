export const environment = {
    production: false,
    test: false,
    dev: true,
    apiBase: 'https://ue.dev.app.spixii.ai/cxd_mockup/app',
};
