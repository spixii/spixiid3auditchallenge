export const environment = {
  production: true,
  test: false,
  dev: false,
  apiBase: 'https://ue.prod.app.spixii.ai/cxd_mockup/app',
};
